# Frontend app of 3c payments

<!-- toc -->

- [Install](#install)
- [Running](#running)
    - [Development mode](#development-mode)
- [Development](#development)
  - [Getting started](#getting-started)
  - [How to make requests?](#how-to-make-requests?)
  - [Nav link with prefetch page](#nav-link-with-prefetch-page)
- [Testing](#testing)

<!-- tocstop -->

## Install

> Requires [Node.js](https://nodejs.org/en/) (see package.json engines.node) and [Yarn](https://classic.yarnpkg.com/en/docs/cli/run).

Install dependencies

```bash
yarn install
```

Copy `.env.example` to `.env`,

```bash
cp .env.example .env
```

Set up your key's values inside the `.env` file if needed.

## Running

### Development mode

Run `dev` script for start app in development mode,

```
yarn dev
```

Open [`http://localhost:9000`](http://localhost:9000) in browser.

## Development

### Getting started

1. Create the file `users.tsx` in the `src/pages` directory
```tsx
import type { FunctionComponent } from "react";
import type { Users } from "src/types";

interface Props {
  data: Users;
}

const UsersPage: FunctionComponent<Props> = ({ data }) => (
  <ul>
    {data.map(({ id, name, email }) => (
      <li key={id}>
        <strong>{name}</strong> <small>{email}</small>
      </li>
    ))}
  </ul>
);

export default UsersPage;
```
> `props.data` — Initialized data from store (optional parameter) equal `useSelector((store: Store) => store.users.data);`

2. Connect the `users` page in the `src/routes.ts` file

```tsx
import { page } from "src/libs/page";
import { requestProduct } from "src/store/actions/product";
import { requestProducts } from "src/store/actions/products";
import { requestUsers } from "src/store/actions/users";

const routes = [
  page("/", "home"),
  page("/products", "products", requestProducts),
  page("/products/:id", "product", requestProduct),
  page("/users", "users", requestUsers),
  page("*", "page-not-found"),
];

export { routes };
```
> `requestUsers` - Initial action (Redux) for initialize page (optional parameter)

3. Connect the reducer in the file `src/reducer.ts`

```ts
import { combineReducers } from "@reduxjs/toolkit";

import product from "src/store/reducers/product";
import products from "src/store/reducers/products";
import users from "src/store/reducers/users";

import type { AnyAction } from "redux";
import type { RouteNames } from "src/types";

export const reducer = combineReducers<
  Partial<
    Record<RouteNames, (state: { data: never }, action: AnyAction) => void>
  >
>({
  product,
  products,
  users,
});
```
> The name of the reducer must match the name of the page.\
State object must contain the required `data` field.

### How to make requests?

1. Create the file `payout.ts` in the `libs/api/routes` directory
```ts
export const payout = {
  "payout.requests": "/api/v1/payout_requests",
  "payout.requests.cancel": "/api/v1/payout_requests/{id}/cancel",
} as const;
```
> `payout.requests.cancel` - Human-readable key\
`/api/v1/payout_requests/{id}/cancel` - Paths key from api-schema.ts\
`{id}` - Replaceable path variable

2. Connect the `payout` routes in the `libs/api/routes.ts` file
```ts
import { common } from "src/libs/api/routes/common";
import { payout } from "src/libs/api/routes/payout";
import { users } from "src/libs/api/routes/users";

export const routes = {
  ...common,
  ...payout,
  ...users,
} as const;
```
3. Call request/useRequest with parameters
```ts
const { response } = await request("payout.requests", "get", {
  query: {
    page: "1",
    per_page: "20",
  },
});
const { response } = await request("payout.requests", "post", {
  data: { address: "address", amount: "amount", comment: "comment" },
});
const { response } = await request("payout.requests.cancel", "post", {
  params: {
    id: "1",
  },
});

const { data } = useRequest("payout.requests.cancel", "post", {
  params: { id: "1" },
});
```
> `params` - Parameters for replace path variables\
`data` - Request body for POST method\
`query` - Parameters for forming a query string (ex.: `?page=1&per_page=20`) 


### Nav link with prefetch page

Prefetch page on mouse over

```tsx
import { PrefetchPageLink } from "src/libs/PrefetchPageLink";

<PrefetchPageLink
  to="/products"
  className={({ isActive: active }) =>
    active ? style.active : undefined
  }
>
  {t("products")}
</PrefetchPageLink>
```

## Testing

Run all tests

```bash
yarn test
```
