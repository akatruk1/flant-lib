/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line import/no-namespace
import type * as _ from "redux";

declare module "redux" {
  export type AnyReducersMapObject<
    S = any,
    A extends AnyAction = any,
  > = ReducersMapObject<S, A>;

  export type AnyReducer<S = any, A extends AnyAction = any> = Reducer<S, A>;

  export interface ReducerManager<M> {
    getReducerMap: () => M;
    reduce: Reducer<
      CombinedState<StateFromReducersMapObject<M>>,
      ActionFromReducersMapObject<M>
    >;
    add: (key: string, reducer: AnyReducer) => Promise<void>;
    remove: (key: string) => Promise<void>;
  }

  export interface Store {
    reducerManager: ReducerManager;
  }
}
