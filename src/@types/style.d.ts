import "styled-components";

import type { TStyledTheme } from "@3commas/ui-kit";

declare module "styled-components" {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  export interface DefaultTheme extends TStyledTheme<Foo> {}
}
