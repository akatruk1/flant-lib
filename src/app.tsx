import { ThemeProvider, lightTheme } from "@3commas/ui-kit";
import { useEffect, useState, Suspense } from "react";
import { Route, Routes, useLocation } from "react-router-dom";

import { routes } from "@config/routes";
import { Loading } from "@modules/layout/moleculas";
import { BaseLayout } from "@modules/layout/templates";
import { GlobalStyles } from "@modules/layout/theme";
import { ProtectedRoute } from "src/lib/router";

export const App = () => {
  const { pathname } = useLocation();

  const [currentPathname, setPathname] = useState(pathname);

  useEffect(() => {
    if (currentPathname !== pathname) {
      window.scrollTo(0, 0);

      setPathname(pathname);
    }
  }, [pathname]);

  return (
    <ThemeProvider theme={lightTheme}>
      <GlobalStyles />

      <BaseLayout>
        <Suspense fallback={<Loading timeout={300} />}>
          <Routes>
            {routes.map(({ path, protect, component: Component }, index) => (
              <Route
                key={index}
                path={path}
                element={
                  protect ? (
                    <ProtectedRoute>
                      <Component />
                    </ProtectedRoute>
                  ) : (
                    <Component />
                  )
                }
              />
            ))}
          </Routes>
        </Suspense>
      </BaseLayout>
    </ThemeProvider>
  );
};
