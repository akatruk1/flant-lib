import { useState } from "react";
import { useTranslation } from "react-i18next";
import styled from "styled-components";

import { DashboardHeader } from "@modules/dashboard/organisms";
import { DocumentTitle } from "@modules/layout/atoms";
import {
  usePagePagination,
  EmptyTransactions,
} from "@modules/layout/organisms";
import { PageStyled } from "@modules/layout/styled";
import { PER_PAGE_OPTIONS } from "@modules/payouts/constants/table";
import { useGetPayoutsCollectionQuery } from "@modules/payouts/model";
import { PayoutFilters } from "@modules/payouts/moleculas";
import {
  CreateTransactionDrawer,
  PayoutsTable,
} from "@modules/payouts/organisms";

import type { FunctionComponent } from "react";

const Filters = styled.div`
  margin-bottom: 24px;
`;

const DashboardPage: FunctionComponent = () => {
  const [openDrawer, setOpenDrawer] = useState(false);

  const handleOpenDrawer = () => setOpenDrawer(true);
  const handleCloseDrawer = () => setOpenDrawer(false);

  const { t } = useTranslation("dashboard");
  const title = t("transaction_history");

  const [currentFilter, setCurrentFilter] = useState("");

  const handleChangeFilter = (type: string) => {
    onChangePage(1);
    onChangePerPage(PER_PAGE_OPTIONS[0]);
    setCurrentFilter(type);
  };

  const { page, perPage, onChangePage, onChangePerPage } = usePagePagination({
    perPage: PER_PAGE_OPTIONS[0],
    page: 1,
  });

  const { isLoading, data: { payoutRequests = [], pagination } = {} } =
    useGetPayoutsCollectionQuery({
      page: String(page),
      per_page: String(perPage),
      state: currentFilter,
    });

  const isShowTable = payoutRequests.length || currentFilter || isLoading;

  return (
    <>
      <PageStyled.Root>
        {isShowTable ? (
          <>
            <Filters>
              <PayoutFilters
                currentFilter={currentFilter}
                onChange={handleChangeFilter}
              />
            </Filters>

            <DocumentTitle title={title} />
            <DashboardHeader title={title} />
            <PayoutsTable
              payoutRequests={payoutRequests}
              page={page}
              perPage={perPage}
              hasMore={!!pagination?.has_more_items}
              onChangePage={onChangePage}
              onChangePerPage={onChangePerPage}
            />
          </>
        ) : (
          <EmptyTransactions onOpenTransaction={handleOpenDrawer} />
        )}
      </PageStyled.Root>
      <CreateTransactionDrawer
        open={openDrawer}
        onClose={handleCloseDrawer}
        onSuccess={handleOpenDrawer}
      />
    </>
  );
};

export default DashboardPage;
