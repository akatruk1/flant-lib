import { Button } from "@3commas/ui-kit";
import { useTranslation } from "react-i18next";
import styled from "styled-components";

import { PrefetchPageLink } from "@lib/router";
import { PageStyled } from "@modules/layout/styled";

import type { FunctionComponent } from "react";

const Root = styled(PageStyled.Root)`
  justify-content: center;

  h1 {
    font-size: 3.6rem;
    text-align: center;
  }
`;

const HomePage: FunctionComponent = () => {
  const { t } = useTranslation();

  return (
    <Root>
      <h1>{t("starter:hello")}</h1>

      <Button size="medium" variant="primary">
        <PrefetchPageLink to="/dashboard">To Dashboard</PrefetchPageLink>
      </Button>
    </Root>
  );
};

export default HomePage;
