import { PageNotFound } from "@modules/layout/organisms";

import type { FunctionComponent } from "react";

const NotFoundPage: FunctionComponent = () => <PageNotFound />;

export default NotFoundPage;
