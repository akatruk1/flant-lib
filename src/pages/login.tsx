import loginImagePath from "@images/loginBackground.png";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";

import { routeFor } from "@lib/router";
import {
  useGetCurrentUserQuery,
  useLoginByGoogleRequestMutation,
} from "@modules/auth/model";
import { WelcomeLogin } from "@modules/auth/organisms";

import type { FunctionComponent } from "react";

const Root = styled.div`
  width: 100%;
  padding: 0 84px;
  background: url(${loginImagePath});
  background-position: top 65px right;
  background-repeat: no-repeat;
`;

const LoginPage: FunctionComponent = () => {
  const navigate = useNavigate();

  const { data: currentUser, isLoading } = useGetCurrentUserQuery();

  const [loginByGoogleRequest] = useLoginByGoogleRequestMutation();

  const handleLoginByGoogleRequest = async () => {
    const result = await loginByGoogleRequest();

    if ("data" in result) {
      const { auth_uri } = result.data;

      window.location.href = auth_uri;
    }
  };

  useEffect(() => {
    if (currentUser) {
      navigate(routeFor("dashboard"));
    }
  }, [currentUser]);

  if (isLoading) {
    return null;
  }

  return (
    <Root>
      <WelcomeLogin
        currentUser={currentUser}
        onClickLoginButton={handleLoginByGoogleRequest}
      />
    </Root>
  );
};

export default LoginPage;
