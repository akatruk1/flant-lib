import { useEffect } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";

import { routeFor } from "@lib/router";
import { useLoginByGoogleMutation } from "@modules/auth/model";

const OauthGooglePage = () => {
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();

  const [loginByGoogle] = useLoginByGoogleMutation();

  const token = localStorage.getItem("token");
  const code = searchParams.get("code");

  useEffect(() => {
    if (code && !token) {
      const loginRequest = async () => {
        const result = await loginByGoogle({ code });

        if ("data" in result) {
          const { jwt } = result.data;

          if (jwt) {
            localStorage.setItem("token", jwt);

            navigate(routeFor("dashboard"));
          }
        }
      };

      loginRequest();
    }
  }, [code, token]);

  return null;
};

export default OauthGooglePage;
