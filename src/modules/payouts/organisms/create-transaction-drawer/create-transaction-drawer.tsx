import { Button } from "@3commas/ui-kit";
import { useState, useRef } from "react";
import { useTranslation } from "react-i18next";
import styled from "styled-components";

import { FormasSubmitButton } from "@lib/formas";
import { Stepper } from "@modules/layout/moleculas";
import { Drawer } from "@modules/layout/organisms";
import { FormStyled } from "@modules/layout/styled";
import {
  CreateTransactionFormas,
  defaultValidationSchema,
  CreateTransactionFormMainGroup,
  CreateTransactionFormDetailsGroup,
} from "@modules/payouts/forms";
import { useCreatePayoutMutation } from "@modules/payouts/model";
import { DiscardChangesGroup } from "@modules/shared/organisms";

import type { DrawerProps } from "@modules/layout/organisms";
import type { CreateTransactionFormProps } from "@modules/payouts/forms";
import type { FunctionComponent } from "react";

type Props = Pick<DrawerProps, "open" | "onClose"> & { onSuccess: () => void };

const Root = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  margin-top: 24px;
`;

const StyledForm = styled(FormStyled.Form)`
  height: 100%;
  margin-top: 24px;
`;

const FormSubmitRow = styled(FormStyled.FormRow)`
  justify-content: space-between;
  margin-top: auto;

  && > * {
    width: calc(50% - 8px);
  }
`;

const BaseButton = styled(Button).attrs({
  type: "button",
  size: "small",
})``;

const DiscardButton = styled(BaseButton).attrs((p) => ({
  disabled: p.submitting,
}))``;

const SubmitButton = styled(BaseButton).attrs((p) => ({
  loading: p.submitting,
  disabled: !p.isValid,
}))``;

const STEPS = [
  "transaction_forms.create.title",
  "transaction_forms.create.details_title",
];

export const CreateTransactionDrawer: FunctionComponent<Props> = ({
  open,
  onClose,
  onSuccess,
}) => {
  const { t } = useTranslation("dashboard");

  const [createPayout] = useCreatePayoutMutation();

  const [currentStep, setCurrentStep] = useState(0);

  const prevStepRef = useRef(0);

  const handleContinueStep = () => setCurrentStep(prevStepRef.current);
  const handleSetDiscardStep = () => setCurrentStep(-1);
  const handleSetDefaultStep = () => setCurrentStep(0);

  const handleNextStep = () =>
    setCurrentStep((state) => {
      const newStep = state + 1;

      prevStepRef.current = newStep;

      return newStep;
    });

  const handleSubmit: CreateTransactionFormProps["onSubmit"] = async (
    values,
  ) => {
    if (currentStep !== 1) {
      handleNextStep();

      return;
    }

    try {
      const { amount, wallet, comment } = values;

      const response = await createPayout({
        address: wallet,
        amount,
        comment,
      });

      if ("data" in response) {
        onClose();
        onSuccess();
      }
    } catch (error) {
      console.error(error);
    }
  };

  const isDiscardStep = currentStep === -1;
  const translatedSteps = STEPS.map((step) => t(step));

  return (
    <Drawer open={open} onClose={onClose} onClosed={handleSetDefaultStep}>
      <Root>
        {isDiscardStep ? null : (
          <Stepper steps={translatedSteps} currentStep={currentStep} />
        )}

        <CreateTransactionFormas
          validateOnChange
          validationSchema={defaultValidationSchema}
          onSubmit={handleSubmit}
        >
          <StyledForm>
            {isDiscardStep ? (
              <DiscardChangesGroup
                title={t("transaction_forms.create.discard.title")}
                subtitle={t("transaction_forms.create.discard.subtitle")}
              />
            ) : null}

            {currentStep === 0 ? (
              <CreateTransactionFormMainGroup title={translatedSteps[0]} />
            ) : null}

            {currentStep === 1 ? (
              <CreateTransactionFormDetailsGroup title={translatedSteps[1]} />
            ) : null}

            {isDiscardStep ? (
              <FormSubmitRow fullWidth>
                <BaseButton
                  key="submit-discard-button"
                  variant="secondary"
                  onClick={onClose}
                >
                  {t("transaction_forms.create.actions.submit_discard")}
                </BaseButton>

                <BaseButton variant="primary" onClick={handleContinueStep}>
                  {t("transaction_forms.create.actions.continue")}
                </BaseButton>
              </FormSubmitRow>
            ) : (
              <FormSubmitRow fullWidth>
                <FormasSubmitButton
                  key="discard-button"
                  variant="secondary"
                  ButtonComponent={DiscardButton}
                  onClick={handleSetDiscardStep}
                >
                  {t("transaction_forms.create.actions.discard")}
                </FormasSubmitButton>

                <FormasSubmitButton
                  variant="primary"
                  ButtonComponent={SubmitButton}
                >
                  {t("transaction_forms.create.actions.next")}
                </FormasSubmitButton>
              </FormSubmitRow>
            )}
          </StyledForm>
        </CreateTransactionFormas>
      </Root>
    </Drawer>
  );
};
