import { Text } from "@3commas/ui-kit";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import styled, { css } from "styled-components";

import { DEFAULT_ORDER } from "@modules/layout/constants/table";
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableSortLabel,
  Paginator,
} from "@modules/layout/organisms";
import { PayoutStatus } from "@modules/payouts/atoms";
import { PER_PAGE_OPTIONS } from "@modules/payouts/constants/table";
import { PayoutActions } from "@modules/payouts/moleculas";

import type { TableHeaderColumn } from "@modules/layout/organisms";
import type { PayoutStatus as PayoutStatusType } from "@modules/payouts/types";
import type { GetPayoutsResponse } from "@modules/shared/api/payouts";
import type { FunctionComponent } from "react";

interface Props {
  payoutRequests: GetPayoutsResponse["payout_requests"];
  page: number;
  perPage: number;
  onChangePage: (page: number) => void;
  onChangePerPage: (perPage: number) => void;
  hasMore?: boolean;
}

const HEADER_COLUMNS: TableHeaderColumn[] = [
  {
    id: "status",
    titleKey: "table_header.status",
  },
  {
    id: "date",
    titleKey: "table_header.date",
  },
  {
    id: "total",
    titleKey: "table_header.total",
  },
  {
    id: "wallet",
    titleKey: "table_header.wallet",
  },
  {
    id: "comment",
    titleKey: "table_header.comment",
  },
  {
    id: "action",
    titleKey: "table_header.action",
    align: "right",
  },
];

const TableContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const StyledTableHeadRow = styled(TableRow)`
  box-shadow: 0 0 2px rgba(27, 28, 29, 0.08), 0 4px 12px rgba(27, 28, 29, 0.04);
`;

const HeadText = styled(Text).attrs({
  group: "body",
  size: "small",
  weight: "medium",
  color: "secondary",
})``;

const BodyText = styled(Text).attrs({
  group: "body",
  size: "small",
  weight: "semibold",
})<{ wordBreak?: boolean }>`
  ${(p) =>
    p.wordBreak &&
    css`
      word-break: break-word;
    `}
`;

export const PayoutsTable: FunctionComponent<Props> = ({
  payoutRequests,
  page,
  perPage,
  hasMore,
  onChangePerPage,
  onChangePage,
}) => {
  const { t } = useTranslation("dashboard");
  const [order, setOrder] = useState(DEFAULT_ORDER);

  const handleChangeOrder = (id: string, direction: string) =>
    setOrder({ orderBy: id, orderDirection: direction });

  return (
    <TableContainer>
      <Table>
        <TableHead>
          <StyledTableHeadRow>
            {HEADER_COLUMNS.map(({ id, titleKey, align, sortable }) => (
              <TableCell type="head" size="small" key={id} align={align}>
                {sortable ? (
                  <TableSortLabel
                    id={id}
                    active={order.orderBy === id}
                    direction={order.orderDirection}
                    onChangeOrder={handleChangeOrder}
                  >
                    <HeadText>{t(titleKey)}</HeadText>
                  </TableSortLabel>
                ) : (
                  <HeadText>{t(titleKey)}</HeadText>
                )}
              </TableCell>
            ))}
          </StyledTableHeadRow>
        </TableHead>

        <TableBody>
          {payoutRequests.map(
            ({
              id,
              state,
              created_at,
              amount,
              address,
              comment,
              transaction_info,
            }) => (
              <TableRow key={id}>
                <TableCell>
                  <PayoutStatus status={state as PayoutStatusType} />
                </TableCell>

                <TableCell>
                  <BodyText>
                    {new Date(created_at).toLocaleDateString("en-GB")}
                  </BodyText>
                </TableCell>

                <TableCell>
                  <BodyText>{amount}</BodyText>
                </TableCell>

                <TableCell>
                  <BodyText>{address}</BodyText>
                </TableCell>

                <TableCell>
                  <BodyText wordBreak>{comment}</BodyText>
                </TableCell>

                <TableCell align="right">
                  <PayoutActions
                    payoutId={id}
                    transactionInfo={transaction_info}
                  />
                </TableCell>
              </TableRow>
            ),
          )}
        </TableBody>
      </Table>

      <Paginator
        mode="cursor"
        page={page}
        perPage={perPage}
        perPageOptions={PER_PAGE_OPTIONS}
        hasMore={hasMore}
        onChangePage={onChangePage}
        onChangePerPage={onChangePerPage}
      />
    </TableContainer>
  );
};
