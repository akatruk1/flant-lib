export type PayoutStatus =
  | "pending"
  | "in_progress"
  | "completed"
  | "rejected"
  | "canceled";

export type PayoutAction =
  | "approve"
  | "cancel"
  | "reject"
  | "complete"
  | "show_info";
