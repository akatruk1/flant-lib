import { useTranslation } from "react-i18next";

import type { PayoutAction } from "@modules/payouts/types";

export const ACTIONS: Record<PayoutAction, string> = {
  cancel: "table_actions.cancel",
  approve: "table_actions.approve",
  complete: "table_actions.complete",
  reject: "table_actions.reject",
  show_info: "table_actions.show_info",
};

export const usePayoutActionName = (actionEvent: PayoutAction) => {
  const { t } = useTranslation("dashboard");

  const actionName = t(ACTIONS[actionEvent]);

  return actionName;
};
