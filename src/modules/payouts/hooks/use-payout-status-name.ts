import { useTranslation } from "react-i18next";

import type { PayoutStatus } from "@modules/payouts/types";

export const STATUS_TITLE_KEY_MAP: Record<PayoutStatus, string> = {
  pending: "table_statuses.pending",
  in_progress: "table_statuses.in_progress",
  completed: "table_statuses.completed",
  rejected: "table_statuses.rejected",
  canceled: "table_statuses.canceled",
};

export const usePayoutStatusName = (state: PayoutStatus) => {
  const { t } = useTranslation("dashboard");

  const statusName = t(STATUS_TITLE_KEY_MAP[state]);

  return statusName;
};
