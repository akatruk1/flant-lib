import { USDTIcon } from "@modules/layout/icons";

export const OPTIONS = [
  {
    label: "USDT trc 20",
    value: "usdt-trc-20",
    icon: <USDTIcon />,
  },
];
