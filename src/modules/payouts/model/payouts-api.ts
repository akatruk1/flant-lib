import { routeForApi } from "@/lib/router/routeFor";
import { appApi } from "@modules/app/store";

import type { PayoutRequest } from "@modules/payouts/entities";
import type {
  GetPayoutRequestBody,
  OnlyIdRequest,
  GetPayoutsResponse,
  GetPayoutEventsResponse,
  GetPayoutEventsRequest,
  CreatePayoutRequest,
} from "@modules/shared/api/payouts";

type GetPayoutsCollectionResult = {
  payoutRequests: GetPayoutsResponse["payout_requests"];
  pagination: GetPayoutsResponse["pagination"];
};

type ChangePayoutStatusRequest = {
  type: "approve" | "cancel" | "reject" | "complete";
} & OnlyIdRequest;

const CHANGE_STATUS_ENDPOINTS = {
  approve: routeForApi("payout.requests.approve"),
  cancel: routeForApi("payout.requests.cancel"),
  reject: routeForApi("payout.requests.reject"),
  complete: routeForApi("payout.requests.complete"),
};

export const payoutsApi = appApi
  .enhanceEndpoints({
    addTagTypes: ["Payouts", "PayoutEvents"],
  })
  .injectEndpoints({
    endpoints: (builder) => ({
      getPayoutsCollection: builder.query<
        GetPayoutsCollectionResult,
        GetPayoutRequestBody
      >({
        query: ({ page, per_page, state }) => ({
          url: routeForApi("payout.requests"),
          params: {
            page,
            per_page,
            state,
          },
        }),

        transformResponse: ({
          payout_requests: payoutRequests,
          pagination,
        }: GetPayoutsResponse) => ({
          payoutRequests,
          pagination,
        }),

        providesTags: (result) => {
          if (result?.payoutRequests) {
            return [
              ...result.payoutRequests.map(
                ({ id }) => ({ type: "Payouts", id } as const),
              ),
              { type: "Payouts", id: "LIST" },
            ];
          }

          return [];
        },
      }),
      getPayoutEvents: builder.query<string[], GetPayoutEventsRequest>({
        query: ({ id }) =>
          routeForApi("payout.requests.events").replace("{id}", String(id)),

        transformResponse: ({ events = [] }: GetPayoutEventsResponse) => events,

        providesTags: (_, __, { id }) => [{ type: "PayoutEvents", id }],
      }),

      createPayout: builder.mutation<PayoutRequest, CreatePayoutRequest>({
        query: (body) => ({
          url: routeForApi("payout.requests"),
          method: "post",
          body,
        }),

        invalidatesTags: [{ type: "Payouts", id: "LIST" }],
      }),
      changePayoutStatus: builder.mutation<void, ChangePayoutStatusRequest>({
        query: ({ type, id }) => ({
          url: CHANGE_STATUS_ENDPOINTS[type].replace("{id}", id),
          method: "post",
        }),

        invalidatesTags: (_, __, { id }) => [
          { type: "Payouts", id },
          { type: "PayoutEvents", id },
        ],
      }),
    }),
  });

export const {
  useGetPayoutsCollectionQuery,
  useGetPayoutEventsQuery,
  useCreatePayoutMutation,
  useChangePayoutStatusMutation,
} = payoutsApi;
