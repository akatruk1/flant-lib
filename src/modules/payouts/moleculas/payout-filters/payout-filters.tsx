import { useTranslation } from "react-i18next";
import styled, { css } from "styled-components";

import type { FunctionComponent } from "react";

type Filter = {
  key: string;
  type: string;
};

type Props = {
  currentFilter: Filter["type"];
  onChange: (type: string) => void;
};

const Root = styled.div`
  display: flex;
  gap: 8px;
`;

const FILTERS: Filter[] = [
  { key: "default_filter_status", type: "" },
  { key: "table_statuses.in_progress", type: "in_progress" },
  { key: "table_statuses.pending", type: "pending" },
  { key: "table_statuses.completed", type: "completed" },
  { key: "table_statuses.canceled", type: "canceled" },
  { key: "table_statuses.rejected", type: "rejected" },
];

const FilterItem = styled.div<{ active?: boolean }>`
  display: flex;
  align-items: center;
  padding: 8px 16px;
  background: ${(p) => p.theme.palette.static.staticWhite};
  border: 1px solid ${(p) => p.theme.palette.static.staticWhite};
  border-radius: 24px;
  cursor: pointer;
  white-space: nowrap;

  > span {
    font-weight: 400;
    color: #19191a;
  }

  &:hover {
    background: #cfdae0;
  }

  ${(p) =>
    p.active &&
    css`
      background: rgba(0, 165, 154, 0.15);
      border: 1px solid #00a59a;
    `}
`;

export const PayoutFilters: FunctionComponent<Props> = ({
  currentFilter,
  onChange,
}) => {
  const { t } = useTranslation("dashboard");

  const translatedFilters = FILTERS.map(({ key, ...otherFilter }) => ({
    name: t(key),
    ...otherFilter,
  }));

  const handleChange = (type: string) => () => onChange(type);

  return (
    <Root>
      {translatedFilters.map(({ name, type }) => (
        <FilterItem
          key={`item-${type}`}
          active={currentFilter === type}
          onClick={handleChange(type)}
        >
          <span>{name}</span>
        </FilterItem>
      ))}
    </Root>
  );
};
