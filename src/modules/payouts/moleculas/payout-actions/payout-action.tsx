import { Text } from "@3commas/ui-kit";
import styled from "styled-components";

import {
  ApproveIcon,
  CancelIcon,
  CompleteIcon,
  InfoIcon,
  RejectIcon,
} from "@modules/layout/icons";
import { usePayoutActionName } from "@modules/payouts/hooks";

import type { PayoutAction as PayoutActionType } from "@modules/payouts/types";
import type { FunctionComponent } from "react";

type Props = {
  action: PayoutActionType;
  onClick: (action: PayoutActionType) => void;
};

const EVENT_COLORS = {
  approve: "#0B8E87",
  cancel: "#D22C40",
  reject: "#D22C40",
  complete: "#0B8E87",
  show_info: "#19191A",
};

const ICONS = {
  cancel: CancelIcon,
  approve: ApproveIcon,
  complete: CompleteIcon,
  reject: RejectIcon,
  show_info: InfoIcon,
} as const;

const Root = styled.div<{ action: PayoutActionType }>`
  display: flex;
  align-items: center;
  gap: 8px;
  padding: 12px 16px;
  white-space: nowrap;
  cursor: pointer;

  p {
    color: ${(p) => EVENT_COLORS[p.action]};
  }
`;

const Icon = styled.span`
  width: 20px;
  height: 20px;
`;

export const PayoutAction: FunctionComponent<Props> = ({ action, onClick }) => {
  const actionName = usePayoutActionName(action);
  const ActionIcon = ICONS[action];

  const handleClick = () => onClick(action);

  return (
    <Root action={action} onClick={handleClick}>
      <Icon>
        <ActionIcon />
      </Icon>

      <Text group="body" size="small" weight="normal">
        {actionName}
      </Text>
    </Root>
  );
};
