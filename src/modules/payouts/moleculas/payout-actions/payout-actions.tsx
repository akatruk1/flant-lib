import { Button } from "@3commas/ui-kit";
import { useEffect, useState } from "react";
import styled from "styled-components";

import { DotsIcon } from "@modules/layout/icons";
import { PopperActions, PopperActionsItem } from "@modules/layout/moleculas";
import {
  useGetPayoutEventsQuery,
  useChangePayoutStatusMutation,
} from "@modules/payouts/model";

import { PayoutAction } from "./payout-action";

import type { PayoutAction as PayoutActionType } from "@modules/payouts/types";
import type { FunctionComponent } from "react";

type Props = {
  payoutId: number;
  transactionInfo: string | null;
};

const Root = styled.div`
  position: relative;
`;

const ButtonTrigger = styled(Button)`
  min-width: initial;
  padding: 4px 8px;

  > span {
    display: inline-flex;
  }
`;

export const PayoutActions: FunctionComponent<Props> = ({
  payoutId,
  transactionInfo,
}) => {
  const id = String(payoutId);

  const [loading, setLoading] = useState(false);
  const [changePayoutStatus] = useChangePayoutStatusMutation();

  const [open, setOpen] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const { data: events = [] } = useGetPayoutEventsQuery(
    { id },
    { skip: !loading },
  );

  const handleClickPayoutAction = (action: PayoutActionType) => {
    if (action !== "show_info") {
      changePayoutStatus({ type: action, id });
    }

    handleClose();
  };

  const handleShowInfo = () => {
    if (transactionInfo) {
      // eslint-disable-next-line no-console
      console.log(transactionInfo, "transactionInfo");
    }
  };

  const handleMouseOver = () => setLoading(true);

  useEffect(() => {
    if (!open || loading) {
      setLoading(false);
    }
  }, [open, loading]);

  return (
    <Root onMouseOver={handleMouseOver}>
      <ButtonTrigger size="small" variant="ghost" onClick={handleOpen}>
        <DotsIcon />
      </ButtonTrigger>

      <PopperActions open={open} onClose={handleClose}>
        {events.map((event) => (
          <PopperActionsItem key={`${id}-${event}`}>
            <PayoutAction
              action={event as PayoutActionType}
              onClick={handleClickPayoutAction}
            />
          </PopperActionsItem>
        ))}

        <PopperActionsItem key={`${id}-show_info`}>
          <PayoutAction action="show_info" onClick={handleShowInfo} />
        </PopperActionsItem>
      </PopperActions>
    </Root>
  );
};
