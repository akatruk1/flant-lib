import styled from "styled-components";

import {
  CanceledIcon,
  CompletedIcon,
  HourglassIcon,
  ProgressIcon,
  RejectedIcon,
} from "@modules/layout/icons";
import { usePayoutStatusName } from "@modules/payouts/hooks";

import type { PayoutStatus as PayoutStatusType } from "@modules/payouts/types";
import type { FunctionComponent } from "react";

type Props = {
  status: PayoutStatusType;
};

const STATUS_COLOR = {
  pending: "#6D6F74",
  in_progress: "#ffa51f",
  completed: "#00a59a",
  rejected: "#d22c40",
  canceled: "#d22c40",
};

const ICONS = {
  pending: HourglassIcon,
  in_progress: ProgressIcon,
  canceled: CanceledIcon,
  completed: CompletedIcon,
  rejected: RejectedIcon,
} as const;

const Root = styled.div<{ status: PayoutStatusType }>`
  display: flex;
  align-items: center;
  gap: 8px;
  color: ${(p) => `${STATUS_COLOR[p.status]}`};
`;

const Icon = styled.span`
  width: 16px;
  height: 16px;
`;

const Title = styled.div`
  font-size: 13px;
  letter-spacing: 0.25px;
  font-weight: 600;
`;

export const PayoutStatus: FunctionComponent<Props> = ({ status }) => {
  const statusName = usePayoutStatusName(status);

  const StatusIcon = ICONS[status];

  return (
    <Root status={status}>
      <Icon>
        <StatusIcon />
      </Icon>

      <Title>{statusName}</Title>
    </Root>
  );
};
