import { Text } from "@3commas/ui-kit";
import { useTranslation } from "react-i18next";

import { FormStyled } from "@modules/layout/styled";
import { FormasInput, FormasTextArea } from "@modules/shared/moleculas";

import { CurrencySelect } from "./currency-select";

import type { FunctionComponent } from "react";

type Props = {
  title: string;
};

export const CreateTransactionFormMainGroup: FunctionComponent<Props> = ({
  title,
}) => {
  const { t } = useTranslation("dashboard");

  return (
    <FormStyled.FormGroup direction="column">
      <FormStyled.FormRow>
        <Text group="title" size="small" weight="normal">
          {title}
        </Text>
      </FormStyled.FormRow>

      <FormStyled.FormRow fullWidth>
        <CurrencySelect />
      </FormStyled.FormRow>

      <FormStyled.FormRow fullWidth>
        <FormasInput
          type="number"
          name="amount"
          size="small"
          label={t("transaction_forms.create.fields.amount.label")}
        />
      </FormStyled.FormRow>

      <FormStyled.FormRow fullWidth>
        <FormasInput
          maxlength={160}
          name="wallet"
          size="small"
          label={t("transaction_forms.create.fields.wallet_address.label")}
        />
      </FormStyled.FormRow>

      <FormStyled.FormRow fullWidth>
        <FormasTextArea
          rows={4}
          name="comment"
          size="small"
          label={t("transaction_forms.create.fields.comment.label")}
        />
      </FormStyled.FormRow>
    </FormStyled.FormGroup>
  );
};
