import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";

import { FormasField } from "@lib/formas";
import { Select } from "@modules/layout/moleculas";
import { OPTIONS } from "@modules/payouts/constants/form";
import { useCreateTransactionFormas } from "@modules/payouts/forms";

import type { TSelectionOption } from "@3commas/ui-kit";
import type { InferProps } from "@lib/formas";

const NAME = "currency";

export const CurrencySelect = () => {
  const { t } = useTranslation("dashboard");

  const {
    setFieldValue,
    selectors: { selectFormCurrency },
  } = useCreateTransactionFormas();

  const value = useSelector(selectFormCurrency());

  const onChange = ({ value, label }: TSelectionOption) =>
    setFieldValue(NAME, { value, label });

  return (
    <FormasField<InferProps<typeof Select>>
      label={t("transaction_forms.create.fields.currency.label")}
      name={NAME}
      options={OPTIONS}
      value={value}
      onOptionSelect={onChange}
      InputComponent={Select}
    />
  );
};
