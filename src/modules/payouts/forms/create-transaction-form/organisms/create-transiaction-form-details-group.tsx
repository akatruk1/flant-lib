import { Text } from "@3commas/ui-kit";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import styled from "styled-components";

import { FormStyled } from "@modules/layout/styled";
import { OPTIONS } from "@modules/payouts/constants/form";
import { useCreateTransactionFormas } from "@modules/payouts/forms";

import type { FunctionComponent } from "react";

type Props = {
  title: string;
};

const DetailsRow = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
`;

const DetailsContentRow = styled(DetailsRow)`
  margin-top: 8px;
`;

const DetailsLabelText = styled(Text).attrs({
  size: "small",
  group: "body",
  weight: "semibold",
})``;

const DetailsContentText = styled(Text).attrs({
  size: "medium",
  group: "body",
  weight: "normal",
})``;

const DetailsIcon = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-right: 8px;
`;

export const CreateTransactionFormDetailsGroup: FunctionComponent<Props> = ({
  title,
}) => {
  const { t } = useTranslation("dashboard");

  const {
    selectors: { selectFormValues },
  } = useCreateTransactionFormas();

  const values = useSelector(selectFormValues);

  const foundOption = OPTIONS.find(
    (option) => option.value === values.currency.value,
  );

  return (
    <FormStyled.FormGroup direction="column">
      <FormStyled.FormRow>
        <Text group="title" size="small" weight="normal">
          {title}
        </Text>
      </FormStyled.FormRow>

      <FormStyled.FormRow fullWidth>
        <DetailsRow>
          <DetailsLabelText>
            {t("transaction_forms.create.fields.currency.label")}
          </DetailsLabelText>
        </DetailsRow>

        <DetailsContentRow>
          {foundOption ? <DetailsIcon>{foundOption.icon}</DetailsIcon> : null}
          <DetailsContentText>{values.currency.label}</DetailsContentText>
        </DetailsContentRow>
      </FormStyled.FormRow>

      <FormStyled.FormRow fullWidth>
        <DetailsRow>
          <DetailsLabelText>
            {t("transaction_forms.create.fields.amount.label")}
          </DetailsLabelText>
        </DetailsRow>

        <DetailsContentRow>
          <DetailsContentText>{values.amount}</DetailsContentText>
        </DetailsContentRow>
      </FormStyled.FormRow>

      <FormStyled.FormRow fullWidth>
        <DetailsRow>
          <DetailsLabelText>
            {t("transaction_forms.create.fields.wallet_address.label")}
          </DetailsLabelText>
        </DetailsRow>

        <DetailsContentRow>
          <DetailsContentText>{values.wallet}</DetailsContentText>
        </DetailsContentRow>
      </FormStyled.FormRow>

      {values.comment ? (
        <FormStyled.FormRow fullWidth>
          <DetailsRow>
            <DetailsLabelText>
              {t("transaction_forms.create.fields.comment.label")}
            </DetailsLabelText>
          </DetailsRow>

          <DetailsContentRow>
            <DetailsContentText>{values.comment}</DetailsContentText>
          </DetailsContentRow>
        </FormStyled.FormRow>
      ) : null}
    </FormStyled.FormGroup>
  );
};
