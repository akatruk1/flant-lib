import { createFormasAdapter } from "@lib/formas";
import { OPTIONS } from "@modules/payouts/constants/form";

import type { CreateTransactionFormValues } from "@modules/payouts/forms";

const id = "createTransactionFormas";

const DEFAULT_VALUES: CreateTransactionFormValues = {
  currency: { value: OPTIONS[0].value, label: OPTIONS[0].label },
  amount: "",
  wallet: "",
  comment: "",
};

const {
  CreateTransactionFormas,
  useCreateTransactionFormas,
  actions: createTransactionFormActions,
  selectors: createTransactionFormSelectors,
} = createFormasAdapter<unknown, CreateTransactionFormValues, typeof id>(
  id,
  DEFAULT_VALUES,
);

export type CreateTransactionFormProps = Parameters<
  typeof CreateTransactionFormas
>[0];

export {
  DEFAULT_VALUES,
  CreateTransactionFormas,
  useCreateTransactionFormas,
  createTransactionFormActions,
  createTransactionFormSelectors,
};
