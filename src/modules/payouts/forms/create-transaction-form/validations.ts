import { object, string, number } from "yup";

import type { TypedValidationSchema } from "@lib/formas";
import type { CreateTransactionFormValues } from "@modules/payouts/forms";

export const defaultValidationSchema: TypedValidationSchema<
  unknown,
  CreateTransactionFormValues
> = () =>
  object({
    currency: object({
      value: string().required(),
    }),
    amount: number().required().moreThan(0).typeError("Must be greater than 0"),
    wallet: string().required().max(160),
  });
