import type { TSelectionOption } from "@3commas/ui-kit";

export type CreateTransactionFormValues = {
  currency: Pick<TSelectionOption, "value" | "label">;
  amount: string;
  wallet: string;
  comment: string;
};
