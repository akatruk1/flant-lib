import type { components } from "api-schema";

export type PayoutRequest = components["schemas"]["payout_request"];
