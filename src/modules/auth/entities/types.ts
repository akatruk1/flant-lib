import type { components } from "api-schema";

export type User = components["schemas"]["user"];
