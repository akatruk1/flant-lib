import { APP_URL } from "@config/environments";
import { routeFor, routeForApi } from "@lib/router/routeFor";
import { appApi } from "@modules/app/store";

import type {
  GetCurrentUserResponse,
  LoginByGoogleRequestResponse,
  LoginByGoogleRequest,
  LoginByGoogleResponse,
} from "@modules/shared/api/auth";

export const authApi = appApi.injectEndpoints({
  endpoints: (builder) => ({
    getCurrentUser: builder.query<GetCurrentUserResponse["user"], void>({
      query: () => routeForApi("current.user"),
      transformResponse: (response: GetCurrentUserResponse) => response.user,
    }),

    loginByGoogleRequest: builder.mutation<LoginByGoogleRequestResponse, void>({
      query: () => ({
        url: routeForApi("oauth.google"),
        method: "get",
        params: {
          redirect_uri: APP_URL + routeFor("oauth/google"),
        },
      }),
    }),

    loginByGoogle: builder.mutation<
      LoginByGoogleResponse,
      LoginByGoogleRequest
    >({
      query: (body) => ({
        url: routeForApi("oauth.google"),
        method: "post",
        body: {
          ...body,
          redirect_uri: APP_URL + routeFor("oauth/google"),
        },
      }),
    }),
  }),
});

export const {
  useGetCurrentUserQuery,
  useLoginByGoogleRequestMutation,
  useLoginByGoogleMutation,
} = authApi;

export const logout = () => {
  localStorage.setItem("token", "");

  return appApi.util.resetApiState();
};
