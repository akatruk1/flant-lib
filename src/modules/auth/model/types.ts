import type { User } from "@modules/auth/entities";

export type AuthSliceState = {
  user: User | null;
};

export type AuthState = {
  auth: AuthSliceState;
};
