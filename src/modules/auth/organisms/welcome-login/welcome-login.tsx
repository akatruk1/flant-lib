import { Button } from "@3commas/ui-kit";
import { useTranslation } from "react-i18next";
import styled from "styled-components";

import type { User } from "@modules/auth/entities";
import type { FunctionComponent } from "react";

type Props = {
  onClickLoginButton: () => void;
  currentUser?: User;
};

const Root = styled.div`
  display: flex;
  flex-direction: column;
  gap: 24px;
  width: 534px;
  margin-top: 176px;
`;

const Title = styled.div`
  font-size: 3.2rem;
  line-height: 44px;
  font-weight: 700;
  letter-spacing: 0.2%;
  color: #19191a;
`;

const Main = styled.div`
  font-size: 2rem;
  line-height: 32px;
  font-weight: 500;
  letter-spacing: 0.3;
  color: #6d6f74;
`;

const Actions = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const WelcomeLogin: FunctionComponent<Props> = ({
  currentUser,
  onClickLoginButton,
}) => {
  const { t } = useTranslation("login");

  return (
    <Root>
      <Title>{t("main_header")}</Title>
      <Main>{t("main_tip")}</Main>

      {currentUser ? null : (
        <Actions>
          <Button
            size="small"
            variant="primary"
            mt={2}
            onClick={onClickLoginButton}
          >
            {t("sign_in")}
          </Button>
        </Actions>
      )}
    </Root>
  );
};
