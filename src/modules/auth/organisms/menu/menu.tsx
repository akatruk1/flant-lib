import { Text, Button } from "@3commas/ui-kit";
import { useTranslation } from "react-i18next";
import styled from "styled-components";

import type { User } from "@modules/auth/entities";
import type { FunctionComponent } from "react";

type Props = {
  onClickLoginButton: () => void;
  currentUser?: User;
};

const Root = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 18px 0;
`;

export const Menu: FunctionComponent<Props> = ({
  currentUser,
  onClickLoginButton,
}) => {
  const { t } = useTranslation("login");

  if (currentUser) {
    return (
      <Root>
        <Text group="body" size="small" weight="normal">
          {`Email: ${currentUser.email}`}
        </Text>
      </Root>
    );
  }

  return (
    <Root>
      <Button size="small" variant="primary" onClick={onClickLoginButton}>
        {t("sign_in")}
      </Button>
    </Root>
  );
};
