import { Button } from "@3commas/ui-kit";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import styled from "styled-components";

import { PageTitle } from "@modules/layout/moleculas";
import { CreateTransactionDrawer } from "@modules/payouts/organisms";
import { SuccessModal } from "@modules/shared/organisms";

import type { PageTitleProps } from "@modules/layout/moleculas";
import type { FunctionComponent } from "react";

type Props = Omit<PageTitleProps, "ActionsComponent">;

const Root = styled(PageTitle)`
  align-items: center;
  height: 84px;
  padding: 0 24px;
  background-color: #fcfcfd;

  > h1 {
    font-size: 2rem;
  }
`;

export const DashboardHeader: FunctionComponent<Props> = ({ title }) => {
  const { t } = useTranslation("dashboard");

  const [successModalOpen, setSuccessModalOpen] = useState(false);

  const handleOpenSuccessModal = () => setSuccessModalOpen(true);
  const handleCloseSuccessModal = () => setSuccessModalOpen(false);

  const [openDrawer, setOpenDrawer] = useState(false);

  const handleOpenDrawer = () => setOpenDrawer(true);
  const handleCloseDrawer = () => setOpenDrawer(false);

  return (
    <Root
      title={title}
      ActionsComponent={
        <>
          <Button size="medium" variant="primary" onClick={handleOpenDrawer}>
            {t("new_transaction")}
          </Button>

          <CreateTransactionDrawer
            open={openDrawer}
            onClose={handleCloseDrawer}
            onSuccess={handleOpenSuccessModal}
          />

          <SuccessModal
            title={t("transaction_forms.create.success.title")}
            content={t("transaction_forms.create.success.content")}
            isOpen={successModalOpen}
            onOpenChange={handleCloseSuccessModal}
          />
        </>
      }
    />
  );
};
