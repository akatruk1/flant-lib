import { createSlice } from "@reduxjs/toolkit";

import type { AppState, AppSliceState } from "./types";
import type { PayloadAction } from "@reduxjs/toolkit";

const initialState = {
  currentSlices: [],
  history: {
    previousPathname: "",
  },
} as AppState["app"];

export const appSlice = createSlice({
  name: "app",
  initialState,
  reducers: {
    storeInjected: (
      state,
      { payload }: PayloadAction<AppSliceState["currentSlices"]>,
    ) => {
      state.currentSlices = state.currentSlices.concat(payload);
    },

    storeRemoved: (
      state,
      { payload }: PayloadAction<AppSliceState["currentSlices"]>,
    ) => {
      state.currentSlices = state.currentSlices.filter(
        (slice) => !payload.includes(slice),
      );
    },

    setHistoryPreviousPathname: (
      state,
      { payload }: PayloadAction<AppSliceState["history"]["previousPathname"]>,
    ) => {
      state.history.previousPathname = payload;
    },
  },
});

export const selectCurrentSlices = (state: AppState) => state.app.currentSlices;
export const selectHistoryPreviousPathname = (state: AppState) =>
  state.app.history.previousPathname;
