import { configureStore as baseConfigureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";

import { createReducerManager } from "@lib/redux";

import { appApi } from "./app-api";
import { appSlice } from "./app-slice";

const rootReducers = {
  [appApi.reducerPath]: appApi.reducer,
  [appSlice.name]: appSlice.reducer,
};

const configureStore = () => {
  const reducerManager = createReducerManager(rootReducers);

  const store = baseConfigureStore({
    devTools: process.env.NODE_ENV !== "production",
    reducer: reducerManager.reduce,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(appApi.middleware),
  });

  store.reducerManager = reducerManager;

  return { store };
};

export const { store } = configureStore();

setupListeners(store.dispatch);
