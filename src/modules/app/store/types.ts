export type AppSliceState = {
  currentSlices: string[];
  history: {
    previousPathname: string;
  };
};

export type AppState = {
  app: AppSliceState;
};
