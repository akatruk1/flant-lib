import { useStore } from "react-redux";

import type { AppState } from "./types";
import type { Store } from "redux";

export const useAppStore: () => Store<AppState> = useStore;
