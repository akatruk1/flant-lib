import { useCallback, useEffect, useMemo, useRef } from "react";

type Handler = (event: MouseEvent | TouchEvent | PointerEvent) => void;

// TODO: This hook took from ui-kit monorepo
// need to create task for export hook from monorepo for public usage
export const useOnClickOutside = (
  handler: Handler,
  { disabled }: { disabled?: boolean } = { disabled: false },
) => {
  const clickCaptured = useRef<boolean>(false);

  const handleInnerClick = useCallback(() => {
    clickCaptured.current = true;
  }, []);

  const handleDocumentClick = useCallback(
    (event: MouseEvent | TouchEvent | PointerEvent) => {
      if (!clickCaptured.current && handler) {
        handler(event);
      }

      clickCaptured.current = false;
    },
    [handler],
  );

  useEffect(() => {
    if (!disabled) {
      document.addEventListener("pointerdown", handleDocumentClick);
    }

    return () => {
      document.removeEventListener("pointerdown", handleDocumentClick);
    };
  }, [handleDocumentClick]);

  return useMemo(
    () => ({
      onPointerDown: disabled ? undefined : handleInnerClick,
    }),
    [handleInnerClick],
  );
};
