import type { paths } from "api-schema";

type OauthGooglePaths = paths["/api/v1/oauth/google"];
type UsersCurrentPaths = paths["/api/v1/users/current"];

export type GetCurrentUserResponse =
  UsersCurrentPaths["get"]["responses"]["200"]["content"]["application/json"];

export type LoginByGoogleRequestResponse =
  OauthGooglePaths["get"]["responses"]["200"]["content"]["application/json"];

export type LoginByGoogleRequest = Pick<
  OauthGooglePaths["post"]["requestBody"]["content"]["application/json"],
  "code"
>;

export type LoginByGoogleResponse =
  OauthGooglePaths["post"]["responses"]["200"]["content"]["application/json"];
