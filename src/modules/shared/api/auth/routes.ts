export const authRoutes = {
  "current.user": "/api/v1/users/current",
  "oauth.google": "/api/v1/oauth/google",
} as const;
