import { authRoutes } from "./auth";
import { commonRoutes } from "./common";
import { payoutRoutes } from "./payouts";

export const apiRoutes = {
  ...commonRoutes,
  ...authRoutes,
  ...payoutRoutes,
};
