import type { paths } from "api-schema";

export type OnlyIdRequest = {
  id: string;
};

export type GetPayoutsResponse =
  paths["/api/v1/payout_requests"]["get"]["responses"]["200"]["content"]["application/json"];

export type GetPayoutEventsResponse =
  paths["/api/v1/payout_requests/{id}/events"]["get"]["responses"]["200"]["content"]["application/json"];

export type GetPayoutEventsRequest =
  paths["/api/v1/payout_requests/{id}/events"]["get"]["parameters"]["path"];

export type GetPayoutRequestBody =
  paths["/api/v1/payout_requests"]["get"]["parameters"]["query"];

export type CreatePayoutRequest =
  paths["/api/v1/payout_requests"]["post"]["requestBody"]["content"]["application/json"];
