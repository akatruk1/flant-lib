export const payoutRoutes = {
  "payout.requests": "/api/v1/payout_requests",
  "payout.requests.approve": "/api/v1/payout_requests/{id}/approve",
  "payout.requests.cancel": "/api/v1/payout_requests/{id}/cancel",
  "payout.requests.complete": "/api/v1/payout_requests/{id}/complete",
  "payout.requests.reject": "/api/v1/payout_requests/{id}/reject",
  "payout.requests.events": "/api/v1/payout_requests/{id}/events",
} as const;
