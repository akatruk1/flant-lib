import { Input } from "@3commas/ui-kit";

import { FormasField } from "@lib/formas";

import type { DefaultFormasFieldProps, InferProps } from "@lib/formas";
import type { FunctionComponent } from "react";

type Props = DefaultFormasFieldProps & InferProps<typeof Input>;

export const FormasInput: FunctionComponent<Props> = (props) => (
  <FormasField<InferProps<typeof Input>> InputComponent={Input} {...props} />
);
