import { TextArea } from "@3commas/ui-kit";
import styled from "styled-components";

import { FormasField } from "@lib/formas";

import type { DefaultFormasFieldProps, InferProps } from "@lib/formas";
import type { FunctionComponent } from "react";

const StyledTextArea = styled(TextArea)`
  textarea {
    resize: none;
  }
` as typeof TextArea;

type Props = DefaultFormasFieldProps & InferProps<typeof StyledTextArea>;

export const FormasTextArea: FunctionComponent<Props> = (props) => (
  <FormasField<InferProps<typeof StyledTextArea>>
    InputComponent={StyledTextArea}
    {...props}
  />
);
