import { Text } from "@3commas/ui-kit";

import { FormStyled } from "@modules/layout/styled";

import type { FunctionComponent, ReactNode } from "react";

type Props = {
  title: string;
  subtitle?: ReactNode;
};

export const DiscardChangesGroup: FunctionComponent<Props> = ({
  title,
  subtitle,
}) => (
  <FormStyled.FormGroup direction="column">
    <FormStyled.FormRow>
      <Text group="title" size="small" weight="normal">
        {title}
      </Text>
    </FormStyled.FormRow>

    {subtitle ? (
      <FormStyled.FormRow>
        <Text group="body" size="medium" weight="normal">
          {subtitle}
        </Text>
      </FormStyled.FormRow>
    ) : null}
  </FormStyled.FormGroup>
);
