import { Text, Modal } from "@3commas/ui-kit";
import styled from "styled-components";

import { SuccessIcon } from "@modules/layout/icons";

import type { TModalProps } from "@3commas/ui-kit";
import type { FunctionComponent, ReactNode } from "react";

type Props = Required<Pick<TModalProps, "isOpen" | "onOpenChange">> & {
  title: string;
  content?: ReactNode;
};

const Icon = styled.div`
  text-align: center;
`;

const Content = styled.div`
  margin-top: 24px;
  text-align: center;
`;

export const SuccessModal: FunctionComponent<Props> = ({
  isOpen,
  onOpenChange,
  title,
  content,
}) => (
  <Modal isOpen={isOpen} onOpenChange={onOpenChange}>
    <Icon>
      <SuccessIcon />
    </Icon>

    <Content>
      <Text group="title" size="medium" weight="semibold">
        {title}
      </Text>

      <Text mt={4} group="body" size="medium" weight="normal">
        {content}
      </Text>
    </Content>
  </Modal>
);
