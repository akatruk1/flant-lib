import type { ReactNode } from "react";

export type PageTitleProps = {
  title: string;
  className?: string;
  ActionsComponent?: ReactNode;
};
