export { PageTitle } from "./page-title";

export type { PageTitleProps } from "./types";
