import styled from "styled-components";

import type { PageTitleProps } from "./types";
import type { FunctionComponent } from "react";

const Root = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
`;

const Actions = styled.div`
  margin-left: auto;
`;

export const PageTitle: FunctionComponent<PageTitleProps> = ({
  title,
  className,
  ActionsComponent,
}) => (
  <Root className={className}>
    <h1>{title}</h1>

    {ActionsComponent ? <Actions>{ActionsComponent}</Actions> : null}
  </Root>
);
