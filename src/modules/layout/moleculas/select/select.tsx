import {
  Dropdown,
  DropdownTrigger,
  DropdownContent,
  DropdownItem,
  Input,
} from "@3commas/ui-kit";
import { useMemo, useCallback, useState } from "react";
import styled from "styled-components";

import { DownChevronIcon } from "@modules/layout/icons";
import { useOnClickOutside } from "@modules/shared/hooks";

import type { TInputProps, TSelectionOption } from "@3commas/ui-kit";
import type { FunctionComponent, ChangeEvent } from "react";

type Props = Omit<TInputProps, "value"> & {
  value: TSelectionOption;
  options: TSelectionOption[];
  onOptionSelect: (option: TSelectionOption) => void;
  error?: unknown;
};

const UpChevronIcon = styled(DownChevronIcon)`
  transform: rotate(180deg);
`;

const InputIconPrefix = styled.div`
  margin-right: -8px;
`;

const OptionIconPrefix = styled.div`
  display: flex;
  margin-right: 8px;
`;

export const Select: FunctionComponent<Props> = ({
  name,
  label,
  size = "small",
  placeholder = "Search",
  options = [],
  value,
  error,
  onChange,
  onBlur,
  onOptionSelect,
}) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const handleOpenDropdown = () => setDropdownOpen(true);
  const handleCloseDropdown = () => setDropdownOpen(false);

  const bindOutsideClick = useOnClickOutside(handleCloseDropdown);

  const handleChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      setDropdownOpen(true);

      if (typeof onChange !== "undefined") {
        onChange(event);
      }
    },
    [onChange],
  );

  const handleSelect = (option: TSelectionOption) => () => {
    setDropdownOpen(false);

    if (typeof onOptionSelect !== "undefined") {
      onOptionSelect(option);
    }
  };

  const renderedPrefix = useMemo(() => {
    const foundOption = options.find((option) => option.value === value.value);

    if (foundOption?.icon) {
      return <InputIconPrefix>{foundOption.icon}</InputIconPrefix>;
    }

    return;
  }, [options, value.value]);

  const renderedSuffix = useMemo(() => {
    if (dropdownOpen) {
      return <UpChevronIcon />;
    }

    return <DownChevronIcon />;
  }, [dropdownOpen]);

  return (
    <Dropdown isHoverable={false} isOpen={dropdownOpen}>
      <DropdownTrigger>
        {(dropdownTriggerOptions) => (
          <Input
            fullWidth
            readOnly
            name={name}
            error={!!error}
            ref={dropdownTriggerOptions.ref}
            label={label}
            size={size}
            value={value.label}
            prefix={renderedPrefix}
            suffix={renderedSuffix}
            placeholder={placeholder}
            onChange={handleChange}
            onClick={handleOpenDropdown}
            onBlur={onBlur}
          />
        )}
      </DropdownTrigger>

      <DropdownContent matchTriggerWidth {...bindOutsideClick}>
        {options.map((option) => (
          <DropdownItem
            role="option"
            size={size}
            key={option.value}
            onClick={handleSelect(option)}
          >
            <OptionIconPrefix>{option.icon}</OptionIconPrefix>
            {option.label}
          </DropdownItem>
        ))}
      </DropdownContent>
    </Dropdown>
  );
};
