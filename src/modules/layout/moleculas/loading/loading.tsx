import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import styled from "styled-components";

import type { FunctionComponent } from "react";

type Props = {
  timeout?: number;
};

const Root = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;

  > strong {
    font-size: 4rem;
  }
`;

export const Loading: FunctionComponent<Props> = ({ timeout }) => {
  const { t } = useTranslation("common");

  const [active, setActive] = useState(false);
  const [timer, setTimer] = useState<NodeJS.Timeout | null>(null);

  useEffect(() => {
    if (timeout) {
      setTimer(setTimeout(() => setActive(true), timeout));
    } else {
      setActive(true);
    }

    return () => {
      if (timer) {
        clearTimeout(timer);
      }
    };
  }, []);

  if (active) {
    return (
      <Root>
        <strong>{t("loading")}</strong>
      </Root>
    );
  }

  return null;
};
