import styled from "styled-components";

import { useOnClickOutside } from "@modules/shared/hooks";

import type { FunctionComponent, ReactNode } from "react";

type Props = {
  open: boolean;
  children: ReactNode;
  onClose: () => void;
};

const Root = styled.div`
  display: flex;
  flex-direction: column;
  position: absolute;
  top: 50px;
  right: 0;
  padding: 8px 0;
  background: ${(p) => p.theme.palette.static.staticWhite};
  border-radius: 4px;
  box-shadow: 0px 0px 4px rgba(27, 28, 29, 0.04),
    0px 8px 24px rgba(27, 28, 29, 0.16);
  z-index: ${(p) => p.theme.layout.zIndices.portal};
`;

export const PopperActions: FunctionComponent<Props> = ({
  open,
  children,
  onClose,
}) => {
  const bindClickOutside = useOnClickOutside(onClose);

  if (!open) {
    return null;
  }

  return <Root {...bindClickOutside}>{children}</Root>;
};
