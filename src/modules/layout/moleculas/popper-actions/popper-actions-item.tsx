import styled from "styled-components";

import type { FunctionComponent, ReactNode } from "react";

type Props = {
  children: ReactNode;
  className?: string;
};

const Root = styled.div`
  display: flex;
  flex-wrap: wrap;
  cursor: pointer;
  transition: background-color 0.2s;

  &:hover {
    background-color: ${(p) => p.theme.palette.static.gray20};
  }
`;

export const PopperActionsItem: FunctionComponent<Props> = ({
  children,
  className,
}) => <Root className={className}>{children}</Root>;
