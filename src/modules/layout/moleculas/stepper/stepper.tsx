import { Text } from "@3commas/ui-kit";
import { useTranslation } from "react-i18next";
import styled, { css } from "styled-components";

import type { FunctionComponent } from "react";

type Props = {
  steps: string[];
  currentStep: number;
};

const Root = styled.div<{ gridCounts: number }>`
  display: grid;
  grid-template-columns: ${(p) => `repeat(${p.gridCounts}, 1fr)`};
  align-items: flex-end;
  width: 100%;
  gap: 4px;
`;

const Step = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
`;

const StepLabel = styled(Text).attrs({
  group: "body",
  size: "micro",
  weight: "semibold",
})`
  color: ${(p) => p.theme.palette.base.primary.normal.default.fill};
  font-size: 1rem;
  line-height: 1;
  text-transform: uppercase;
`;

const StepIndicator = styled.div<{ active?: boolean }>`
  width: 100%;
  height: 2px;
  margin-top: 8px;

  ${(p) =>
    (p.active &&
      css`
        background-color: ${p.theme.palette.base.primary.normal.default.fill};
      `) ||
    css`
      background-color: ${p.theme.palette.base.primary.normal.disabled.fill};
    `}
`;

export const Stepper: FunctionComponent<Props> = ({ steps, currentStep }) => {
  const { t } = useTranslation("common");

  if (steps.length === 0) {
    return null;
  }

  return (
    <Root gridCounts={steps.length}>
      {steps.map((step, index) => {
        const stepKey = step.replace(" ", "-");
        const isFirstStep = index === 0;

        return (
          <Step key={stepKey}>
            {isFirstStep ? (
              <StepLabel>
                {t("stepper.step_label", {
                  currentStep: currentStep + 1,
                  countSteps: steps.length,
                })}
              </StepLabel>
            ) : null}

            <StepIndicator active={index === currentStep} />
          </Step>
        );
      })}
    </Root>
  );
};
