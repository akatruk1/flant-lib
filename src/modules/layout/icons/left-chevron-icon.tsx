import LeftChevron from "./source/left-chevron.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const LeftChevronIcon: FunctionComponent<IconProps> = (props) => (
  <LeftChevron {...props} />
);
