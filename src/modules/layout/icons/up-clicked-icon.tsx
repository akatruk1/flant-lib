import UpClicked from "./source/up-clicked.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const UpClickedIcon: FunctionComponent<IconProps> = (props) => (
  <UpClicked {...props} />
);
