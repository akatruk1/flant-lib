import Complete from "./source/complete.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const CompleteIcon: FunctionComponent<IconProps> = (props) => (
  <Complete {...props} />
);
