import EmptyTransactions from "./source/empty-transactions.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const EmptyTransactionsIcon: FunctionComponent<IconProps> = (props) => (
  <EmptyTransactions {...props} />
);
