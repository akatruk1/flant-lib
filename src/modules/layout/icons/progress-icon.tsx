import Progress from "./source/progress.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const ProgressIcon: FunctionComponent<IconProps> = (props) => (
  <Progress {...props} />
);
