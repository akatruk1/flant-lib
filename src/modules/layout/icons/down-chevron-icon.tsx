import DownChevron from "./source/down-chevron.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const DownChevronIcon: FunctionComponent<IconProps> = (props) => (
  <DownChevron {...props} />
);
