import Rejected from "./source/rejected.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const RejectedIcon: FunctionComponent<IconProps> = (props) => (
  <Rejected {...props} />
);
