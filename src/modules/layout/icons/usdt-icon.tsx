import Usdt from "./source/usdt-icon.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const USDTIcon: FunctionComponent<IconProps> = (props) => (
  <Usdt {...props} />
);
