import Close from "./source/close.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const CloseIcon: FunctionComponent<IconProps> = (props) => (
  <Close {...props} />
);
