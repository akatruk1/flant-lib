import Avatar from "./source/avatar.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const AvatarIcon: FunctionComponent<IconProps> = (props) => (
  <Avatar {...props} />
);
