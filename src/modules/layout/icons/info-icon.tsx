import Info from "./source/info.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const InfoIcon: FunctionComponent<IconProps> = (props) => (
  <Info {...props} />
);
