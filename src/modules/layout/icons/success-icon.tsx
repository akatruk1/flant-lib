import Success from "./source/success.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const SuccessIcon: FunctionComponent<IconProps> = (props) => (
  <Success {...props} />
);
