import Down from "./source/down.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const DownIcon: FunctionComponent<IconProps> = (props) => (
  <Down {...props} />
);
