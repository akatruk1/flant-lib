import Dots from "./source/dots.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const DotsIcon: FunctionComponent<IconProps> = (props) => (
  <Dots {...props} />
);
