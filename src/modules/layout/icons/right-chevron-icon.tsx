import RightChevron from "./source/right-chevron.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const RightChevronIcon: FunctionComponent<IconProps> = (props) => (
  <RightChevron {...props} />
);
