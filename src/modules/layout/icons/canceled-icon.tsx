import Canceled from "./source/canceled.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const CanceledIcon: FunctionComponent<IconProps> = (props) => (
  <Canceled {...props} />
);
