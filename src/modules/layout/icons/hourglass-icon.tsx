import Hourglass from "./source/hourglass.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const HourglassIcon: FunctionComponent<IconProps> = (props) => (
  <Hourglass {...props} />
);
