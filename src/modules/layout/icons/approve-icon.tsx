import Approve from "./source/approve.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const ApproveIcon: FunctionComponent<IconProps> = (props) => (
  <Approve {...props} />
);
