import Completed from "./source/completed.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const CompletedIcon: FunctionComponent<IconProps> = (props) => (
  <Completed {...props} />
);
