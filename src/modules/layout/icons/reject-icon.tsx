import Reject from "./source/reject.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const RejectIcon: FunctionComponent<IconProps> = (props) => (
  <Reject {...props} />
);
