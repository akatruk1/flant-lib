import Cancel from "./source/cancel.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const CancelIcon: FunctionComponent<IconProps> = (props) => (
  <Cancel {...props} />
);
