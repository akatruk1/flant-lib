import DownClicked from "./source/down-clicked.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const DownClickedIcon: FunctionComponent<IconProps> = (props) => (
  <DownClicked {...props} />
);
