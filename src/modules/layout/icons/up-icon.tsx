import Up from "./source/up.svg";

import type { IconProps } from "./types";
import type { FunctionComponent } from "react";

export const UpIcon: FunctionComponent<IconProps> = (props) => (
  <Up {...props} />
);
