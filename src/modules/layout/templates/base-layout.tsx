import styled from "styled-components";

import { Header } from "@modules/layout/organisms";

import type { FunctionComponent, ReactNode } from "react";

type Props = {
  children: ReactNode;
};

const Root = styled.main`
  display: flex;
  flex-wrap: wrap;
  align-items: flex-start;
  width: 100%;
  height: 100%;
`;

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  min-height: calc(100% - 46px);
  padding: 32px 56px;
  background-color: #f7f7f9;
`;

export const BaseLayout: FunctionComponent<Props> = ({ children }) => (
  <Root>
    <Header />
    <Container>{children}</Container>
  </Root>
);
