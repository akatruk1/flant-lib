import styled from "styled-components";

import { PaginatorControls } from "./paginator-controls";
import { PaginatorPerPage } from "./paginator-per-page";

import type { PaginatorPropsByMode } from "./types";
import type { FunctionComponent, ReactNode } from "react";

type Props = PaginatorPropsByMode & {
  perPageOptions: number[];
  onChangePerPage: (perPage: number) => void;
  ActionsComponent?: ReactNode;
};

const Root = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  gap: 24px;
  padding: 24px 16px;
  background: ${(p) => p.theme.palette.static.staticWhite};
`;

const Actions = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  margin-left: auto;
`;

const getPaginatorPerPageProps = (options: Props) => {
  const { mode, page, perPage, perPageOptions, onChangePage, onChangePerPage } =
    options;

  const baseProps = {
    page,
    perPage,
    perPageOptions,
    onChangePage,
    onChangePerPage,
  };

  if (mode === "page") {
    return {
      mode: "page" as const,
      total: options.total,
      ...baseProps,
    };
  }

  return {
    mode: "cursor" as const,
    hasMore: options.hasMore,
    ...baseProps,
  };
};

const getPaginatorControlsProps = (options: Props) => {
  const { mode, page, perPage, onChangePage } = options;

  const baseProps = {
    page,
    perPage,
    onChangePage,
  };

  if (mode === "page") {
    return {
      mode: "page" as const,
      total: options.total,
      ...baseProps,
    };
  }

  return {
    mode: "cursor" as const,
    disableNextButton: !options.hasMore,
    ...baseProps,
  };
};

export const Paginator: FunctionComponent<Props> = (props) => {
  const { ActionsComponent } = props;

  const paginatorPerPageProps = getPaginatorPerPageProps(props);
  const paginatorControlsProps = getPaginatorControlsProps(props);

  return (
    <Root>
      <PaginatorPerPage {...paginatorPerPageProps} />
      {ActionsComponent ? <Actions>{ActionsComponent}</Actions> : null}
      <PaginatorControls {...paginatorControlsProps} />
    </Root>
  );
};
