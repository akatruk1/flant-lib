import {
  Text,
  Dropdown,
  DropdownItem,
  DropdownTrigger,
  DropdownContent,
} from "@3commas/ui-kit";
import { useState, useEffect } from "react";
import styled from "styled-components";

import { DownChevronIcon } from "@modules/layout/icons";
import { useOnClickOutside } from "@modules/shared/hooks";

import type { PaginatorPropsByMode } from "./types";
import type { FunctionComponent } from "react";

type Props = PaginatorPropsByMode & {
  perPageOptions: number[];
  onChangePerPage: (perPage: number) => void;
};

const Root = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  gap: 24px;
`;

const PerPage = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  min-width: 75px;
  height: 36px;
  padding: 8px 12px;
  background: #fcfcfd;
  border: 1px solid #c8cbd0;
  border-radius: 4px;
  cursor: pointer;
`;

const PerPageIcon = styled.div`
  width: 16px;
  height: 16px;
`;

export const PaginatorPerPage: FunctionComponent<Props> = (props) => {
  const { mode, page, perPage, perPageOptions, onChangePage, onChangePerPage } =
    props;

  const [openPerPage, setOpenPerPage] = useState(false);

  const handleOpenPerPage = () => setOpenPerPage(true);
  const handleClosePerPage = () => setOpenPerPage(false);

  const handleChangePerPage = (perPage: number) => () => {
    onChangePerPage(perPage);
    handleClosePerPage();
  };

  useEffect(() => {
    onChangePage(1);
  }, [perPage]);

  const bindOutsideClick = useOnClickOutside(handleClosePerPage);
  const isPageMode = mode === "page";

  const getRangeOfRecords = () => {
    const currentMaxRecords = page * perPage;
    const currentRecords = currentMaxRecords - perPage + 1;

    if (!isPageMode) {
      return `Showing ${currentRecords} - ${currentMaxRecords}`;
    }

    const { total } = props;

    const currentTotalRecords =
      total > currentMaxRecords ? currentMaxRecords : total;

    return `Showing ${currentRecords} - ${currentTotalRecords} of ${total}`;
  };

  return (
    <Root>
      <Dropdown isOpen={openPerPage} isHoverable={false}>
        <DropdownTrigger>
          {({ ref }) => (
            <PerPage ref={ref} onClick={handleOpenPerPage}>
              <Text mr={4} group="body" size="small" weight="normal">
                {perPage}
              </Text>

              <PerPageIcon>
                <DownChevronIcon />
              </PerPageIcon>
            </PerPage>
          )}
        </DropdownTrigger>

        <DropdownContent {...bindOutsideClick}>
          {perPageOptions.map((perPageOption) => (
            <DropdownItem
              size="small"
              key={perPageOption}
              onClick={handleChangePerPage(perPageOption)}
            >
              {perPageOption}
            </DropdownItem>
          ))}
        </DropdownContent>
      </Dropdown>

      <Text group="body" size="small" weight="normal" color="secondary">
        {getRangeOfRecords()}
      </Text>
    </Root>
  );
};
