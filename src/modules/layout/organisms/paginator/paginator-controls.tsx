import { Button } from "@3commas/ui-kit";
import styled from "styled-components";

import { LeftChevronIcon, RightChevronIcon } from "@modules/layout/icons";

import type { PaginatorPropsByMode } from "./types";
import type { FunctionComponent } from "react";

type Props = PaginatorPropsByMode & {
  siblingCount?: number;
  disablePrevButton?: boolean;
  disableNextButton?: boolean;
};

const Root = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  gap: 4px;
`;

const ButtonControl = styled(Button).attrs({
  size: "small",
})`
  min-width: 36px;
  padding: 4px 8px;

  > span {
    display: inline-flex;
  }
`;

const Icon = styled.div<{ active: boolean }>`
  display: inline-flex;

  color: ${(p) =>
    p.active
      ? p.theme.palette.content.primary.default
      : p.theme.palette.content.primary.disabled};
`;

const PaginatorControlItem = styled(ButtonControl).attrs({
  size: "small",
})`
  min-width: 36px;
`;

const range = (start: number, end: number) => {
  const length = end - start;

  return Array.from({ length }, (_, index) => start + index + 1);
};

export const PaginatorControls: FunctionComponent<Props> = (props) => {
  const {
    mode,
    page,
    perPage,
    siblingCount = 3,
    disablePrevButton,
    disableNextButton,
    onChangePage,
  } = props;

  const isPageMode = mode === "page";

  const totalPages = isPageMode ? Math.ceil(props.total / perPage) : 0;
  const activePrevButton =
    typeof disablePrevButton === "boolean" ? !disablePrevButton : page > 1;

  const activeNextButton =
    typeof disableNextButton === "boolean"
      ? !disableNextButton
      : page < totalPages;

  const handleChangePage = (page: number) => () => onChangePage(page);

  const handleClickPrev = () => {
    if (activePrevButton) {
      onChangePage(page - 1);
    }
  };

  const handleClickNext = () => {
    if (activeNextButton) {
      onChangePage(page + 1);
    }
  };

  const pagesBatchIdx = Math.floor((page - 1) / siblingCount);
  const startPagesIdx = siblingCount * pagesBatchIdx;
  const endPagesIdx = siblingCount * (pagesBatchIdx + 1);

  const pages = range(
    startPagesIdx,
    endPagesIdx > totalPages ? totalPages : endPagesIdx,
  );

  const cursorMode = mode === "cursor";

  return (
    <Root>
      <ButtonControl
        variant="ghost"
        disabled={!activePrevButton}
        onClick={handleClickPrev}
      >
        <Icon active={activePrevButton}>
          <LeftChevronIcon />
        </Icon>
      </ButtonControl>

      {cursorMode
        ? null
        : pages.map((pageItem) => (
            <PaginatorControlItem
              key={pageItem}
              variant={page === pageItem ? "primary" : "ghost"}
              onClick={handleChangePage(pageItem)}
            >
              {pageItem}
            </PaginatorControlItem>
          ))}

      <ButtonControl
        variant="ghost"
        disabled={!activeNextButton}
        onClick={handleClickNext}
      >
        <Icon active={activeNextButton}>
          <RightChevronIcon />
        </Icon>
      </ButtonControl>
    </Root>
  );
};
