import { useCallback, useState } from "react";

type Options = {
  perPage: number;
  page?: number;
};

export const usePagePagination = ({ page = 1, perPage }: Options) => {
  const [interPage, setInternalPage] = useState(page);

  const handleChangePage = useCallback(
    (page: number) => setInternalPage(page),
    [],
  );

  const [internalPerPage, setInternalPerPage] = useState(perPage);

  const handleChangePerPage = useCallback(
    (perPage: number) => setInternalPerPage(perPage),
    [],
  );

  return {
    page: interPage,
    perPage: internalPerPage,
    onChangePage: handleChangePage,
    onChangePerPage: handleChangePerPage,
  };
};
