export type PaginatorMode = "page" | "cursor";

export type BasePaginatorProps = {
  page: number;
  perPage: number;
  onChangePage: (page: number) => void;
};

export type PaginatorPropsByMode = (
  | {
      mode: "page";
      total: number;
    }
  | {
      mode: "cursor";
      hasMore?: boolean;
    }
) &
  BasePaginatorProps;
