import { Text, Modal } from "@3commas/ui-kit";
import { useTranslation } from "react-i18next";

import type { TModalProps } from "@3commas/ui-kit";
import type { FunctionComponent } from "react";

type Props = Required<Pick<TModalProps, "isOpen" | "onOpenChange">>;

export const AboutModal: FunctionComponent<Props> = ({
  isOpen,
  onOpenChange,
}) => {
  const { t } = useTranslation("starter");

  return (
    <Modal title={t("about_3c")} isOpen={isOpen} onOpenChange={onOpenChange}>
      <Text group="body" size="large" weight="normal">
        {t("starter:description")}
      </Text>

      <Text mt={6} group="body" size="large" weight="normal">
        {t("starter:developed_by")}
        <a href="https://github.com/ogarich89" target="_blank" rel="noreferrer">
          ogarich89
        </a>
      </Text>
    </Modal>
  );
};
