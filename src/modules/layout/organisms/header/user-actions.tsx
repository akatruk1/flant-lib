import { Button } from "@3commas/ui-kit";
import { useTranslation } from "react-i18next";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import styled, { css } from "styled-components";

import { SUPPORTED_LANGUAGES, DEFAULT_LANGUAGE } from "@/i18n";
import { logout } from "@modules/auth/model";

import type { FunctionComponent } from "react";

type Props = {
  userId?: number;
  className?: string;
};

const Root = styled.div`
  display: flex;
  align-items: center;
  gap: 64px;
`;

const Language = styled.div`
  display: flex;
  gap: 16px;
`;

const LanguageItem = styled.li<{ active?: boolean }>`
  font-size: 1.6rem;
  font-weight: 600;
  text-transform: uppercase;
  cursor: pointer;

  &:hover {
    color: ${(p) => p.theme.palette.static.staticBlack};
  }

  ${(p) =>
    (p.active &&
      css`
        color: ${(p) => p.theme.palette.content.primary.default};
      `) ||
    css`
      color: ${(p) => p.theme.palette.content.secondary.disabled};
    `}
`;

const ButtonLogout = styled(Button).attrs({
  size: "medium",
  variant: "ghost",
})`
  color: ${(p) => p.theme.palette.content.secondary.default};
`;

export const UserActions: FunctionComponent<Props> = ({
  userId,
  className,
}) => {
  const { i18n, t } = useTranslation("common");

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleChangeLanguage = (language: string) => async () =>
    i18n.changeLanguage(language);

  const handleLogout = () => {
    dispatch(logout());
    navigate("login");
  };

  return (
    <Root className={className}>
      <Language>
        {SUPPORTED_LANGUAGES.map((language) => (
          <LanguageItem
            key={language}
            active={language === (i18n.language || DEFAULT_LANGUAGE)}
            onClick={handleChangeLanguage(language)}
          >
            {language}
          </LanguageItem>
        ))}
      </Language>

      {userId ? (
        <ButtonLogout color="s" onClick={handleLogout}>
          {t("logout")}
        </ButtonLogout>
      ) : null}
    </Root>
  );
};
