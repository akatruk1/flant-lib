import styled from "styled-components";

import { useGetCurrentUserQuery } from "@modules/auth/model";

import { UserActions } from "./user-actions";
import { UserInfo } from "./user-info";

import type { FunctionComponent } from "react";

const Root = styled.div`
  padding: 0 56px;
  background: #f7f7f9;
  width: 100%;
  min-width: 1152px;
`;

const Container = styled.div`
  margin: 56px 0;
  padding: 6px 0;
  display: flex;
  justify-content: space-between;
  gap: 20px;
`;

const StyledUserActions = styled(UserActions)`
  margin-left: auto;
` as typeof UserActions;

export const Header: FunctionComponent = () => {
  const { data: currentUser } = useGetCurrentUserQuery();

  return (
    <Root>
      <Container>
        {currentUser ? <UserInfo email={currentUser.email} /> : null}

        <StyledUserActions userId={currentUser?.id} />
      </Container>
    </Root>
  );
};
