import { Text } from "@3commas/ui-kit";
import styled from "styled-components";

import { AvatarIcon } from "@modules/layout/icons";

import type { FunctionComponent } from "react";

type Props = {
  email: string;
};

const Root = styled.div`
  display: flex;
  align-items: center;
  gap: 12px;
`;

const Name = styled(Text).attrs({
  group: "body",
  size: "medium",
  weight: "semibold",
})``;

export const UserInfo: FunctionComponent<Props> = ({ email }) => (
  <Root>
    <AvatarIcon />
    <Name>{email}</Name>
  </Root>
);
