import styled, { css } from "styled-components";

import {
  DownIcon,
  UpIcon,
  UpClickedIcon,
  DownClickedIcon,
} from "@modules/layout/icons";

import type { OrderDirection } from "./types";
import type { FunctionComponent, ReactNode } from "react";

interface Props {
  id: string;
  active: boolean;
  direction: string;
  children: ReactNode;
  onChangeOrder: (id: string, direction: OrderDirection) => void;
}

const Title = styled.div``;

const Controls = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 16px;
  height: 16px;
  gap: 3px;
`;

const Root = styled.div<{ active?: boolean }>`
  display: inline-flex;
  flex-wrap: wrap;
  align-items: center;
  gap: 12px;
  cursor: pointer;
  user-select: none;

  ${(p) =>
    p.active &&
    css`
      > ${Title}, p {
        color: ${(p) => p.theme.palette.content.primary.default};
        font-weight: 700;
      }
    `}
`;

export const TableSortLabel: FunctionComponent<Props> = ({
  id,
  active,
  direction,
  children,
  onChangeOrder,
}) => {
  const handleClick = () => {
    let newId = id;
    let newDirection: OrderDirection = "";

    if (direction === "") {
      newDirection = "asc";
    } else if (direction === "asc") {
      newDirection = "desc";
    } else if (direction === "desc") {
      newId = "";
    }

    onChangeOrder(newId, newDirection);
  };

  const renderControls = () => {
    if (!active || !direction) {
      return (
        <>
          <UpIcon />
          <DownIcon />
        </>
      );
    }

    if (direction === "asc") {
      return (
        <>
          <UpClickedIcon />
          <DownIcon />
        </>
      );
    }

    if (direction === "desc") {
      return (
        <>
          <UpIcon />
          <DownClickedIcon />
        </>
      );
    }
  };

  return (
    <Root active={active} onClick={handleClick}>
      <Title>{children}</Title>

      <Controls>{renderControls()}</Controls>
    </Root>
  );
};
