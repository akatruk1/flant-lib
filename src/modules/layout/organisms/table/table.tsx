import styled from "styled-components";

import type { FunctionComponent, ReactNode } from "react";

type Props = {
  children: ReactNode;
  className?: string;
};

const Root = styled.div``;

const StyledTable = styled.table`
  width: 100%;
  border-collapse: collapse;
  background: ${(p) => p.theme.palette.static.staticWhite};
`;

export const Table: FunctionComponent<Props> = ({ className, children }) => (
  <Root className={className}>
    <StyledTable>{children}</StyledTable>
  </Root>
);
