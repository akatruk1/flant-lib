export type TableCellType = "head" | "body";
export type TableCellSize = "medium" | "small";
export type TableCellAlign = "left" | "center" | "right";

export type TableHeaderColumn = {
  id: string;
  titleKey: string;
  align?: TableCellAlign;
  sortable?: boolean;
};

export type OrderDirection = "asc" | "desc" | "";
