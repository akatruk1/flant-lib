import styled from "styled-components";

import type { FunctionComponent, ReactNode } from "react";

type Props = {
  children: ReactNode;
  className?: string;
};

const Root = styled.tr``;

export const TableRow: FunctionComponent<Props> = ({ className, children }) => (
  <Root className={className}>{children}</Root>
);
