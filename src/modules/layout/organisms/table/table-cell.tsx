import styled from "styled-components";

import type { TableCellType, TableCellSize, TableCellAlign } from "./types";
import type { FunctionComponent, ReactNode } from "react";

type Props = {
  children: ReactNode;
  type?: TableCellType;
  size?: TableCellSize;
  align?: TableCellAlign;
  className?: string;
};

const TYPE_AS_MAP: Record<TableCellType, "th" | "td"> = {
  head: "th",
  body: "td",
};

const SIZE_MAP: Record<TableCellSize, number> = {
  medium: 60,
  small: 44,
};

const Root = styled.td.withConfig({
  shouldForwardProp: (prop, defaultValidatorFn) =>
    defaultValidatorFn(prop) || ["align"].includes(prop),
})<{ size: TableCellSize }>`
  height: ${(p) => SIZE_MAP[p.size]};
  min-width: 130px;
  padding: 12px 16px;
`;

export const TableCell: FunctionComponent<Props> = ({
  type = "body",
  size = "medium",
  align = "left",
  className,
  children,
}) => (
  <Root as={TYPE_AS_MAP[type]} size={size} align={align} className={className}>
    {children}
  </Root>
);
