import styled from "styled-components";

import type { FunctionComponent } from "react";

const Root = styled.section`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;

  > strong {
    font-size: 4rem;
  }
`;

export const PageNotFound: FunctionComponent<unknown> = () => (
  <Root>
    <strong>PAGE NOT FOUND</strong>
  </Root>
);
