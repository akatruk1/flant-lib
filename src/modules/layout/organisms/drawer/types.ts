import type { ReactNode } from "react";

export type DrawerProps = {
  open: boolean;
  onClose: () => void;
  closeIcon?: ReactNode;
  className?: string;
  children?: ReactNode;
  onClosed?: () => void;
};
