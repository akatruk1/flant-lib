import { Button } from "@3commas/ui-kit";
import { useEffect } from "react";
import { CSSTransition } from "react-transition-group";
import styled from "styled-components";

import { CloseIcon } from "@modules/layout/icons";

import { Portal } from "./portal";

import type { DrawerProps } from "./types";
import type { FunctionComponent } from "react";

const TRANSITION_CLASSNAME = "fade";
const ANIMATION_DURATION = 300;

const Overlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 90;
  background-color: rgba(60, 61, 63, 0.5);
  transition: background-color ${ANIMATION_DURATION}ms ease;
`;

const Main = styled.div`
  position: fixed;
  display: flex;
  flex-direction: column;
  top: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  max-width: 580px;
  padding: 32px;
  background-color: ${(p) => p.theme.palette.static.staticWhite};
  overflow: scroll;
  z-index: 95;
`;

const Root = styled(Portal)`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 95;

  &.fade-enter {
    & ${Overlay} {
      animation: backgroundTransitionIn ease forwards 0.3s;
    }

    & ${Main} {
      animation: slideRightIn ease forwards 0.3s;
    }
  }

  &.fade-exit {
    & ${Overlay} {
      background-color: transparent;
      animation: backgroundTransitionOut ease forwards 0.3s;
    }

    & ${Main} {
      animation: slideRightOut ease forwards 0.3s;
    }
  }

  @keyframes slideRightIn {
    0% {
      transform: translateX(100%);
    }

    100% {
      transform: translateX(0);
    }
  }

  @keyframes slideRightOut {
    0% {
      transform: translateX(0);
    }

    100% {
      transform: translateX(100%);
    }
  }

  @keyframes backgroundTransitionIn {
    0% {
      background-color: transparent;
    }

    100% {
      background-color: rgba(60, 61, 63, 0.5);
    }
  }

  @keyframes backgroundTransitionOut {
    0% {
      background-color: rgba(60, 61, 63, 0.5);
    }

    100% {
      background-color: transparent;
    }
  }
`;

const Header = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
`;

const CloseButton = styled(Button)`
  display: flex;
  justify-content: center;
  margin-left: auto;
  min-width: 0;
  padding: 4px;
  color: #6b7b8b;

  &:hover {
    color: #6b7b8b;
  }

  > svg {
    width: 32px;
    height: 32px;
  }
`;

export const Drawer: FunctionComponent<DrawerProps> = ({
  open,
  onClose,
  onClosed,
  closeIcon = <CloseIcon />,
  className,
  children,
}) => {
  useEffect(() => {
    const closeOnEscape = (e: KeyboardEvent) => {
      document.body.style.overflow = "hidden";

      if (e.key === "Escape") {
        onClose();
      }
    };

    if (open) {
      document.body.style.overflow = "hidden";
      document.addEventListener("keydown", closeOnEscape);
    }

    return () => {
      document.body.style.removeProperty("overflow");
      document.removeEventListener("keydown", closeOnEscape);
    };
  }, [open]);

  return (
    <CSSTransition
      unmountOnExit
      mountOnEnter={false}
      classNames={TRANSITION_CLASSNAME}
      timeout={ANIMATION_DURATION}
      onExited={onClosed}
      in={open}
    >
      <Root className={className}>
        <Overlay onClick={onClose} />

        <Main>
          <Header>
            {closeIcon ? (
              <CloseButton
                size="small"
                variant="ghost"
                icon={closeIcon}
                onClick={onClose}
              />
            ) : null}
          </Header>

          {children}
        </Main>
      </Root>
    </CSSTransition>
  );
};
