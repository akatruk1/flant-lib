import { createPortal } from "react-dom";

import type { FunctionComponent, ReactNode } from "react";

type Props = {
  children: ReactNode;
  className?: string;
};

export const Portal: FunctionComponent<Props> = ({ className, children }) =>
  createPortal(<div className={className}>{children}</div>, document.body);
