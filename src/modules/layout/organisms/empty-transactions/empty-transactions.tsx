import { Button, Text } from "@3commas/ui-kit";
import { useTranslation } from "react-i18next";
import styled from "styled-components";

import { EmptyTransactionsIcon } from "@modules/layout/icons";

import type { FunctionComponent } from "react";

const ContentWrapper = styled.div`
  background: #f7f7f9;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Container = styled.div`
  width: 344px;
  height: 339px;
  display: flex;
  flex-direction: column;
  gap: 19px;
  align-items: center;
  margin-bottom: 300px;
`;

const IconWrapper = styled.div`
  width: 148px;
  height: 148px;
`;

const TextWrapper = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;
  gap: 16px;
  margin-bottom: 21px;
  white-space: pre-line;
`;

interface Props {
  onOpenTransaction: () => void;
}

export const EmptyTransactions: FunctionComponent<Props> = ({
  onOpenTransaction,
}) => {
  const { t } = useTranslation("dashboard");

  return (
    <ContentWrapper>
      <Container>
        <IconWrapper>
          <EmptyTransactionsIcon />
        </IconWrapper>
        <TextWrapper>
          <Text group="title" size="small" weight="semibold">
            {t("empty_transactions")}
          </Text>
          <Text group="body" size="medium" weight="normal" color="secondary">
            {t("empty_transactions_hint")}
          </Text>
        </TextWrapper>
        <Button
          size="medium"
          variant="primary"
          px={6}
          py={3}
          onClick={onOpenTransaction}
        >
          {t("common:new_transaction")}
        </Button>
      </Container>
    </ContentWrapper>
  );
};
