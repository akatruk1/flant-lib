import styled from "styled-components";

export const PageStyled = {
  Root: styled.section`
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
    width: 100%;
  `,
  Main: styled.div`
    display: flex;
    flex-wrap: wrap;
    width: 100%;
    margin-top: 32px;
  `,
};
