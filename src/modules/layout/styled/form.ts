import styled, { css } from "styled-components";

const FormRow = styled.div<{ fullWidth?: boolean }>`
  display: flex;
  flex-wrap: wrap;
  align-items: center;

  ${(p) =>
    p.fullWidth &&
    css`
      width: 100%;

      && > * {
        width: 100%;
      }
    `}
`;

export const FormStyled = {
  Form: styled.form<{ direction?: "row" | "column" }>`
    display: flex;
    flex-wrap: wrap;
    flex-direction: ${(p) => p.direction ?? "row"};

    & > *:not(:last-child) {
      margin-bottom: 20px;
    }
  `,

  FormGroup: styled.div<{ direction?: "row" | "column" }>`
    display: flex;
    flex-wrap: wrap;
    flex-direction: ${(p) => p.direction ?? "row"};
    width: 100%;

    & > *:not(:last-child) {
      margin-bottom: 20px;
    }
  `,

  FormRow,
};
