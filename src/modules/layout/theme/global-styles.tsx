import { NormalizeCss } from "@3commas/ui-kit";
import { createGlobalStyle, css } from "styled-components";

const globals = css`
  html,
  body,
  #root {
    height: 100%;
  }

  html {
    box-sizing: border-box;
    font-size: 10px;
    color: ${(p) => p.theme.palette.content.primary.default};

    &.hidden {
      height: 100%;
      overflow-y: scroll;

      body {
        height: 100%;
        overflow: hidden;
      }
    }
  }

  #root {
    display: flex;
    flex-wrap: wrap;
  }

  body {
    min-width: 320px;
    margin: 0 auto;
    overflow-x: hidden;
    overflow-y: scroll;
    font-family: "Open Sans", sans-serif;
  }

  menu,
  ol,
  ul {
    list-style: none;
    margin: 0;
    padding: 0;
  }

  li {
    list-style: none;
  }

  *,
  *:before,
  *:after {
    box-sizing: inherit;
  }

  a,
  button,
  input,
  select,
  option,
  textarea {
    outline: none;
    border: 0;
  }

  button {
    background-color: transparent;
    padding: 0;
  }

  input {
    appearance: none;
    -webkit-border-radius: 0;
  }

  button::-moz-focus-inner {
    border: 0;
  }

  input[type="file"] {
    display: none;
  }

  a img {
    border: 0;
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  p {
    margin: 0;
    line-height: 1em;
  }

  h1,
  h2 {
    font-family: "Open Sans", sans-serif;
    font-weight: 700;
  }

  h1 {
    font-size: 2.6rem;
  }

  h2 {
    font-size: 2rem;
  }

  h3,
  h4 {
    font-family: Ubuntu, sans-serif;
  }

  h3 {
    font-size: 2rem;
    font-weight: 700;
  }

  h4 {
    font-size: 1.4rem;
    font-weight: 700;
  }
`;

const Styles = createGlobalStyle`${globals}`;

export const GlobalStyles = () => (
  <>
    <NormalizeCss />
    <Styles />
  </>
);
