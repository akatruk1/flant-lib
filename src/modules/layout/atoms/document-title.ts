import type { FunctionComponent } from "react";

type Props = {
  title: string;
};

export const DocumentTitle: FunctionComponent<Props> = ({ title }) => {
  if (document.title !== title) {
    document.title = title;
  }

  return null;
};
