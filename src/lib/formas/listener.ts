/* eslint-disable @typescript-eslint/no-explicit-any */
import { getProperty } from "dot-prop";

import type {
  ListenerIdentifierComplex,
  Listener,
  CreateListenerResult,
  ListenerManagerOptions,
  ListenerEventOption,
  _ListenerExecutionHelpers,
  _ChangeDataProvideWatcher,
  _ChangeDataProvideWatchers,
  _ChangeValueProvideWatchers,
  _GetWatcherForResult,
} from "./types";

export const createListener = <State, Values>(): CreateListenerResult<
  State,
  Values
> => {
  const listeners: Listener<State, Values>[] = [];

  return {
    add: function (identifier, options) {
      listeners.push({ identifier, options } as Listener<State, Values>);

      return this;
    },
    build: () => listeners,
  };
};

export enum ListenerEvents {
  OnProvideValue = 0,
  OnChangeValue = 1,
}

export const LISTENER_EVENTS = {
  onProvideValue: {
    id: ListenerEvents.OnProvideValue,
    priority: 0,
  },
  onChangeValue: {
    id: ListenerEvents.OnChangeValue,
    priority: 1,
  },
};

export class ListenerManager<State, Values> {
  #listeners: Listener<State, Values>[] = [];
  #beforeEachListener: Listener<State, Values> | undefined;
  #afterEachListener: Listener<State, Values> | undefined;
  #observeListeners: Listener<State, Values>[] = [];
  #changeDataProviderWatchers: _ChangeDataProvideWatchers<State, Values> = [];
  #changeValueProviderWatchers: _ChangeValueProvideWatchers<State, Values> = {};

  #config: ListenerManagerOptions<State, Values> | undefined;

  set listeners(listeners) {
    this.#listeners = listeners;
    this.#beforeEachListener = listeners.find(
      ({ identifier }) => identifier === "beforeEach",
    );

    this.#afterEachListener = listeners.find(
      ({ identifier }) => identifier === "afterEach",
    );

    this.#observeListeners = listeners.filter(
      ({ options }) => !!options.observe,
    );

    this.#changeDataProviderWatchers = this._getWatchersFor("data");
    this.#changeValueProviderWatchers = this._getWatchersFor("values");
  }

  get listeners() {
    return this.#listeners;
  }

  set config(config) {
    this.#config = config;
  }

  get config() {
    return this.#config;
  }

  trigger(event: ListenerEvents, options?: ListenerEventOption) {
    switch (event) {
      case LISTENER_EVENTS.onProvideValue.id:
        this.run(
          () => this.executeOnProvideValueListener(),
          LISTENER_EVENTS.onProvideValue.priority,
        );
        break;

      case LISTENER_EVENTS.onChangeValue.id:
        if (!options) {
          return;
        }

        this.run(
          () => this.executeOnChangeValueListener(options),
          LISTENER_EVENTS.onChangeValue.priority,
        );
        break;

      default:
        break;
    }
  }

  run(fn: () => void, priority: number) {
    if (priority === 0) {
      fn();
    } else {
      setTimeout(() => fn(), priority * 10);
    }
  }

  executeOnChangeValueListener(options: ListenerEventOption) {
    if (!this.config) {
      return;
    }

    const { key, value, onSetFieldValue } = options;

    const listenerByKey = this.#observeListeners.find(({ identifier }) => {
      if (typeof identifier === "object") {
        return identifier.key === key;
      }

      return identifier === key;
    });

    const valueWatchers = this.#changeValueProviderWatchers[key];

    if (this.#beforeEachListener || listenerByKey || this.#afterEachListener) {
      const {
        selectors,
        helpers: originHelpers,
        initialValues,
        dispatch,
        store,
      } = this.config;

      let helpers = {
        ...originHelpers,
      };

      if (onSetFieldValue) {
        helpers = {
          ...helpers,
          setFieldValue: (key: string, value: any) => {
            originHelpers.setFieldValue(key, value);
            onSetFieldValue(key, value);
          },
        };
      }

      const currentState = store.getState();
      const executionOptions = this._getBaseListenerExecutionOptions({
        key,
        value,
        helpers,
        initialValues,
        values: selectors.selectFormValues(currentState),
        dispatch,
        state: currentState,
      });

      if (this.#beforeEachListener) {
        const typedBeforeAllFn = this.#beforeEachListener.options
          .observe as NonNullable<
          Listener<State, Values>["options"]["observe"]
        >;

        typedBeforeAllFn(executionOptions);
      }

      if (listenerByKey) {
        const typedObserveFn = listenerByKey.options.observe as NonNullable<
          Listener<State, Values>["options"]["observe"]
        >;

        typedObserveFn(executionOptions);
      }

      if (valueWatchers) {
        valueWatchers.forEach((watcher) => {
          const typedObserveFn = watcher.provide as NonNullable<
            Listener<State, Values, string, string[]>["options"]["provide"]
          >;

          const watcherExecutionOptions = this._getBaseListenerExecutionOptions(
            {
              key: watcher.key,
              value: selectors.selectFormValue(currentState, watcher.key),
              helpers,
              initialValues,
              values: selectors.selectFormValues(currentState),
              dispatch,
              state: currentState,
            },
          );

          typedObserveFn({
            dependsData: undefined,
            ...watcherExecutionOptions,
          });
        });
      }

      if (this.#afterEachListener) {
        const typedAfterAllFn = this.#afterEachListener.options
          .observe as NonNullable<
          Listener<State, Values>["options"]["observe"]
        >;

        typedAfterAllFn(executionOptions);
      }
    }
  }

  executeOnProvideValueListener() {
    if (!this.config) {
      return;
    }

    const {
      selectors,
      helpers,
      initialValues,
      dispatch,
      store,
      getPreviousValue,
      setPreviousValue,
    } = this.config;

    this.#changeDataProviderWatchers.forEach(([dependsDataKey, watchers]) => {
      const currentState = store.getState();

      const dependsPreviousValue = getPreviousValue(dependsDataKey);
      const dependsCurrentValue: any = getProperty(
        currentState,
        dependsDataKey,
      );

      if (dependsPreviousValue !== dependsCurrentValue) {
        setPreviousValue(dependsDataKey, dependsCurrentValue);

        watchers.forEach((watcher) => {
          const { key, provide, dataAlias, enableScenarios } = watcher;

          if (!enableScenarios.includes("existInitialValue")) {
            if (initialValues[key as keyof typeof initialValues]) {
              return;
            }
          }

          const currentValue = selectors.selectFormValue(currentState, key);

          const dependsData = dataAlias
            ? { [dataAlias]: dependsCurrentValue }
            : undefined;

          const executionOptions = this._getBaseListenerExecutionOptions({
            key,
            value: currentValue,
            helpers,
            initialValues,
            values: selectors.selectFormValues(currentState),
            dispatch,
            state: currentState,
          });

          const typedProvideFn = provide as NonNullable<
            Listener<State, Values, string, any>["options"]["provide"]
          >;

          typedProvideFn({ dependsData, ...executionOptions });
        });
      }
    });
  }

  _getWatchersFor<T extends "data" | "values">(
    dependsType: T,
  ): _GetWatcherForResult<State, Values, T> {
    const listeners = this._getProvideListenersFor(dependsType);

    if (dependsType === "data") {
      const watchers = listeners.reduce<
        Record<string, _ChangeDataProvideWatcher<State, Values>[]>
      >((carry, { identifier, options }) => {
        const { key, dependsOn } = identifier as Required<
          ListenerIdentifierComplex<string, any[], never>
        >;

        const { provide, enableProvide = [] } = options;

        const typedProvideFn = provide as NonNullable<
          Listener<State, Values, string, any[]>["options"]["provide"]
        >;

        dependsOn.forEach((dependsItem) => {
          let path = dependsItem;
          let dataAlias = undefined;

          if (typeof dependsItem === "object" && "path" in dependsItem) {
            path = dependsItem.path;
            dataAlias = dependsItem.as;
          }

          const watcher = {
            key,
            provide: typedProvideFn,
            dataAlias,
            enableScenarios: enableProvide,
          };

          if (typeof carry[path] === "undefined") {
            carry[path] = [watcher];
          } else {
            const currentItem = carry[path];

            carry[path] = [...currentItem, watcher];
          }
        });

        return carry;
      }, {});

      return Object.entries(watchers) as _GetWatcherForResult<State, Values, T>;
    } else if (dependsType === "values") {
      const watchers = listeners.reduce<
        _ChangeValueProvideWatchers<State, Values>
      >((carry, { identifier, options }) => {
        const { key, dependsOnValues } = identifier as Required<
          ListenerIdentifierComplex<string, never, string[]>
        >;

        const typedProvideFn = options.provide as NonNullable<
          Listener<State, Values, string, string[]>["options"]["provide"]
        >;

        dependsOnValues.forEach((dependsPath) => {
          if (typeof carry[dependsPath] === "undefined") {
            carry[dependsPath] = [{ key, provide: typedProvideFn }];
          } else {
            const currentItem = carry[dependsPath];

            carry[dependsPath] = [
              ...currentItem,
              { key, provide: typedProvideFn },
            ];
          }
        });

        return carry;
      }, {});

      return watchers as _GetWatcherForResult<State, Values, T>;
    }

    return undefined as _GetWatcherForResult<State, Values, T>;
  }

  _getProvideListenersFor(dependsType: "data" | "values") {
    const provideListeners = this.#listeners.filter(
      ({ options }) => options.provide,
    );

    if (dependsType === "data") {
      return provideListeners.filter(
        ({ identifier }) =>
          typeof identifier === "object" && identifier.dependsOn,
      );
    } else if (dependsType === "values") {
      return provideListeners.filter(
        ({ identifier }) =>
          typeof identifier === "object" && identifier.dependsOnValues,
      );
    }

    return [];
  }

  _getBaseListenerExecutionOptions({
    key,
    value,
    helpers,
    initialValues,
    values,
    dispatch,
    state,
  }: _ListenerExecutionHelpers<State, Values>) {
    return {
      target: { key, value },
      helpers: {
        ...helpers,
        setFieldValueSilent: (key: string, value: any) => {
          helpers.setFieldValue(key, value, { observable: false });
        },
      },
      storeOptions: {
        initialValues,
        values,
        dispatch,
        state,
      },
    };
  }
}
