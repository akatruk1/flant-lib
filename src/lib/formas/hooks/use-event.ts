import { useRef, useLayoutEffect, useCallback } from "react";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type Handler = (...args: any[]) => Promise<any> | any;

export const useEvent = <T extends Handler>(fn: T) => {
  const fnRef = useRef<T | null>(null);

  useLayoutEffect(() => {
    fnRef.current = fn;
  });

  return useCallback(
    (async (...args) => {
      if (fnRef.current) {
        await fnRef.current(...args);
      }
    }) as T,
    [],
  );
};
