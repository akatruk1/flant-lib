import type {
  FormasActions,
  FormasSelectors,
  FormasHelpers,
  FormasHandlers,
} from "./form";

export type FormasContextValues<
  State,
  Values,
  Id extends string,
> = FormasHelpers<Values> &
  FormasHandlers & {
    actions: FormasActions<Values, Id>;
    selectors: FormasSelectors<State, Values>;
  };
