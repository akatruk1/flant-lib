import type { ListenerProvideOptions, EnableProvideScenario } from "./listener";

export type _ChangeValueProvideWatcher<State, Values> = {
  key: string;
  provide: (
    options: ListenerProvideOptions<State, Values, string, string[]>,
  ) => void;
};

export type _ChangeValueProvideWatchers<State, Values> = Record<
  string,
  _ChangeValueProvideWatcher<State, Values>[]
>;

export type _ChangeDataProvideWatcher<State, Values> = {
  key: string;
  enableScenarios: EnableProvideScenario[];
  provide: (
    options: ListenerProvideOptions<State, Values, string, string[]>,
  ) => void;
  dataAlias?: string;
};

export type _ChangeDataProvideWatchers<State, Values> = [
  string,
  _ChangeDataProvideWatcher<State, Values>[],
][];

export type _GetWatcherForResult<
  State,
  Values,
  T extends "data" | "values",
> = T extends "data"
  ? _ChangeDataProvideWatchers<State, Values>
  : T extends "values"
  ? _ChangeValueProvideWatchers<State, Values>
  : undefined;
