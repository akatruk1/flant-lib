/* eslint-disable @typescript-eslint/no-explicit-any */
export type Path = (number | string)[];

export type ReadonlyPath = Readonly<Path>;

export type Head<P extends ReadonlyPath> = P extends [] ? never : P[0];

export type Other<P extends ReadonlyPath> = P extends readonly [
  infer _,
  ...infer O,
]
  ? O extends []
    ? never
    : O
  : never;

export type InferableObject = Record<string | number, any> | any[];

export type Infer<
  S extends InferableObject,
  P extends ReadonlyPath,
> = Head<P> extends keyof S
  ? Other<P> extends false
    ? S[Head<P>]
    : Infer<S[Head<P>], Other<P> extends ReadonlyPath ? Other<P> : []>
  : never;

export type StrToPath<S> = S extends `${infer Head}.${infer Tail}`
  ? Tail extends string
    ? [Head, ...StrToPath<Tail>]
    : [Head]
  : [S];
