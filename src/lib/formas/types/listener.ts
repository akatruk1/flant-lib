/* eslint-disable @typescript-eslint/no-explicit-any */
import type { FormasHelpers, FormasSelectors } from "./form";
import type { UseCacheManagerResult } from "./helpers";
import type { StrToPath, Infer } from "./ts-helpers";
import type { Store, Dispatch } from "redux";

type Target<Values, Key> = {
  key: Key;
  value: Key extends keyof Values ? Values[Key] : any;
};

type Helpers<Values> = FormasHelpers<Values> & {
  setFieldValueSilent: (key: string, value: any) => void;
};

type StoreOptions<State, Values> = {
  state: State;
  dispatch: Dispatch;
  initialValues: Values;
  values: Values;
};

export type ListenerObserveOptions<State, Values, Key> = {
  target: Target<Values, Key>;
  helpers: Helpers<Values>;
  storeOptions: StoreOptions<State, Values>;
};

type DependsItem = {
  readonly path: string;
  readonly as: string;
};

type DependsUnionToObject<State, D> = D extends DependsItem[]
  ? {
      [Key in D[number] as D[number]["as"]]: Infer<
        State,
        StrToPath<D[number]["path"]>
      >;
    }
  : never;

export type ListenerProvideOptions<State, Values, Key, Depends> = {
  target: Target<Values, Key>;
  dependsData: Depends extends string[]
    ? undefined
    : DependsUnionToObject<State, Depends>;

  helpers: Helpers<Values>;
  storeOptions: StoreOptions<State, Values>;
};

export type ListenerIdentifierSimple<K> = K;
export type ListenerIdentifierComplex<K, D, DV> = {
  key: K;
  dependsOn?: D;
  dependsOnValues?: DV;
};

export type ListenerIdentifier<K, D, DV> =
  | ListenerIdentifierSimple<K>
  | ListenerIdentifierComplex<K, D, DV>;

type InferKeyFromIdentifier<Identifier, K, D, DV> =
  Identifier extends ListenerIdentifierComplex<K, D, DV>
    ? Identifier["key"]
    : Identifier;

type InferDependsFromIdentifier<Identifier, K, D, DV> =
  Identifier extends ListenerIdentifierComplex<K, D, DV>
    ? Identifier["dependsOn"]
    : never;

type InferKey<Key> = Key extends "beforeEach" | "afterEach" ? string : Key;

export type EnableProvideScenario = "existInitialValue";

type ListenerOptions<State, Values, Key, Depends> = {
  observe?: (
    options: ListenerObserveOptions<State, Values, InferKey<Key>>,
  ) => void;

  provide?: (
    options: ListenerProvideOptions<State, Values, InferKey<Key>, Depends>,
  ) => void;

  enableProvide?: EnableProvideScenario[];
};

export type Listener<
  State,
  Values,
  Key = string,
  DependsOn = unknown,
  DependsOnValues = unknown,
> = {
  identifier: ListenerIdentifier<Key, DependsOn, DependsOnValues>;
  options: ListenerOptions<
    State,
    Values,
    InferKeyFromIdentifier<
      ListenerIdentifier<Key, DependsOn, DependsOnValues>,
      Key,
      DependsOn,
      DependsOnValues
    >,
    InferDependsFromIdentifier<
      ListenerIdentifier<Key, DependsOn, DependsOnValues>,
      Key,
      DependsOn,
      DependsOnValues
    >
  >;
};

export type CreateListenerResult<State, Values> = {
  add: <
    Key extends string,
    DependsOn extends DependsItem[] | string[],
    DependsOnValues extends string[],
    Identifier extends ListenerIdentifier<Key, DependsOn, DependsOnValues>,
  >(
    identifier: Identifier,
    options: ListenerOptions<
      State,
      Values,
      InferKeyFromIdentifier<Identifier, Key, DependsOn, DependsOnValues>,
      InferDependsFromIdentifier<Identifier, Key, DependsOn, DependsOnValues>
    >,
  ) => CreateListenerResult<State, Values>;
  build: () => Listener<State, Values>[];
};

export type ListenerManagerOptions<State, Values> = {
  helpers: FormasHelpers<Values>;
  selectors: FormasSelectors<State, Values>;
  initialValues: Values;
  dispatch: Dispatch;
  store: Store;
  setPreviousValue: UseCacheManagerResult["set"];
  getPreviousValue: UseCacheManagerResult["get"];
};

export type _ListenerExecutionHelpers<State, Values> = {
  key: string;
  value: any;
  helpers: FormasHelpers<Values>;
  initialValues: Values;
  values: Values;
  dispatch: Dispatch;
  state: State;
};

export type ListenerEventOption = {
  key: string;
  value: any;
  onSetFieldValue?: (key: string, value: any) => void;
};
