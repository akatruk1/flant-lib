/* eslint-disable @typescript-eslint/no-explicit-any */
import type { FunctionComponent } from "react";

export type InferProps<C> = C extends (...args: any[]) => JSX.Element
  ? Parameters<C>[0]
  : C extends FunctionComponent<infer P>
  ? P
  : never;

export type DefaultFormasSubmitButtonProps = {
  type?: JSX.IntrinsicElements["button"]["type"];
  onClick?: (event: MouseEvent) => void;
};

export type InnerFormasSubmitButtonProps = {
  submitting: boolean;
  isValid: boolean;
};

export type DefaultFormasFieldProps = {
  name: string;
  value?: any;
  ignoreOriginSet?: boolean;
  onChange?: (value: any) => void;
  onBlur?: (event: FocusEvent) => void;
};

export type InnerFormasFieldProps = {
  error: any;
};
