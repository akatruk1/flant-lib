/* eslint-disable @typescript-eslint/no-explicit-any */
import type { InferValueByKey } from "./helpers";
import type { CreateListenerResult } from "./listener";
import type { ActionCreatorWithPayload } from "@reduxjs/toolkit";
import type { ReactNode, FormEvent, ChangeEvent } from "react";
import type { ObjectSchema } from "yup";

export type FormasDefaultValues = { [key: string]: any };
export type FormasError =
  | string
  | { key: string; options: Record<string, any> }
  | Record<string, any>;

export type FormasErrors<S> = Partial<Record<keyof S, FormasError>>;
export type FormasTouched<S> = Record<keyof S, boolean>;

export type ExtractPayload<ActionCreator> = ActionCreator extends (
  payload: infer P,
) => infer _
  ? P
  : never;

export type SetFieldValueOptions = {
  observable?: boolean;
  ignoreOriginSet?: boolean;
};

export type SetFieldValuePayload<S> =
  | {
      [Key in keyof S]: {
        key: Key;
        value: InferValueByKey<S, Key>;
        options?: SetFieldValueOptions;
      };
    }[keyof S]
  | {
      [key: string]: {
        key: string;
        value: InferValueByKey<S, string>;
        options?: SetFieldValueOptions;
      };
    }[string];

export type SetFieldErrorPayload<S> =
  | {
      [Key in keyof S]: { key: Key; value: FormasError };
    }[keyof S]
  | {
      [key: string]: { key: string; value: FormasError };
    }[string];

export type SetFieldTouchPayload<S> =
  | {
      [Key in keyof S]: { key: Key; value: boolean };
    }[keyof S]
  | {
      [key: string]: { key: string; value: boolean };
    }[string];

export type FormasState<Values> = {
  values: Values;
  errors: FormasErrors<Values>;
  touched: FormasTouched<Values>;
  isValid: boolean;
  isSubmitting: boolean;
};

export type FormasActions<Values, Id extends string = string> = {
  setValues: ActionCreatorWithPayload<Values, `${Id}/setValues`>;
  setErrors: ActionCreatorWithPayload<FormasErrors<Values>, `${Id}/setErrors`>;
  setTouched: ActionCreatorWithPayload<
    FormasTouched<Values>,
    `${Id}/setTouched`
  >;
  setFieldValue: ActionCreatorWithPayload<
    SetFieldValuePayload<Values>,
    `${Id}/setFieldValue`
  >;

  setFieldError: ActionCreatorWithPayload<
    SetFieldErrorPayload<Values>,
    `${Id}/setFieldError`
  >;

  setFieldTouch: ActionCreatorWithPayload<
    SetFieldTouchPayload<Values>,
    `${Id}/setFieldTouch`
  >;

  startSubmit: ActionCreatorWithPayload<void, `${Id}/startSubmit`>;
  endSubmit: ActionCreatorWithPayload<void, `${Id}/endSubmit`>;
  resetForm: ActionCreatorWithPayload<
    Partial<Values> | undefined,
    `${Id}/resetForm`
  >;
};

export type FormasPayloadAction<AC> = AC extends ActionCreatorWithPayload<
  infer P,
  infer T
>
  ? { type: T; payload: P }
  : never;

export type FormasHelpers<Values> = {
  setValues: (values: Values, shouldValidate?: boolean) => void;
  setFieldValue: <K extends string>(
    key: K,
    value: InferValueByKey<Values, K>,
    options?: SetFieldValueOptions,
  ) => void;

  setErrors: (errors: FormasErrors<Values>) => void;
  setFieldError: <K extends string>(key: K, value: FormasError) => void;
  setTouched: (touched: FormasTouched<Values>) => void;
  setFieldTouch: <K extends string>(key: K, value: boolean) => void;
  resetForm: (values?: Partial<Values>) => void;
  startSubmit: () => void;
  endSubmit: () => void;
};

export type FormasBag<Values> = Pick<
  FormasState<Values>,
  "touched" | "errors"
> &
  FormasHelpers<Values>;

export type FormasValueGeneratedSelector<State, Values> = {
  [Key in keyof Values as Key extends string
    ? `selectForm${Capitalize<Key>}`
    : never]: (key?: string) => (state: State) => InferValueByKey<Values, Key>;
};

export type FormasErrorGeneratedSelector<State, Values> = {
  [Key in keyof Values as Key extends string
    ? `selectForm${Capitalize<Key>}Error`
    : never]: (key?: string) => (state: State) => FormasError;
};

export type FormasTouchedGeneratedSelector<State, Values> = {
  [Key in keyof Values as Key extends string
    ? `selectForm${Capitalize<Key>}Touch`
    : never]: (key?: string) => (state: State) => boolean;
};

export type FormasSelectors<State, Values> = FormasValueGeneratedSelector<
  State,
  Values
> &
  FormasErrorGeneratedSelector<State, Values> &
  FormasTouchedGeneratedSelector<State, Values> & {
    selectForm: (state: State) => FormasState<Values>;
    selectFormValues: (state: State) => FormasState<Values>["values"];
    selectFormValue: <K extends keyof Values | string>(
      state: State,
      key: K,
    ) => InferValueByKey<Values, K>;

    selectFormErrors: (state: State) => FormasState<Values>["errors"];
    selectFormError: <K extends keyof Values | string>(
      state: State,
      key: K,
    ) => FormasError;

    selectFormTouched: (state: State) => FormasState<Values>["touched"];
    selectFormTouch: <K extends keyof Values | string>(
      state: State,
      key: K,
    ) => boolean;

    selectIsValidForm: (state: State) => boolean;
    selectIsSubmittingForm: (state: State) => boolean;
  };

export type FormasHandlers = {
  handleChange: (event: ChangeEvent) => void;
  handleBlur: (event: FocusEvent) => void;
  handleSubmit: (
    event: FormEvent<HTMLFormElement> | MouseEvent,
  ) => Promise<void>;
};

export type TypedValidationSchema<State, Values> = (
  values: Values,
  state: State,
) => ObjectSchema<any>;

export interface FormasProps<State, Values, Id extends string> {
  id: Id;
  initialValues: Values;
  children:
    | ReactNode
    | ((formasHelpers: FormasHelpers<Values> & FormasHandlers) => ReactNode);

  onSubmit: (
    values: Values,
    formasBag: FormasBag<Values>,
    state: State,
  ) => Promise<void> | void;

  listeners?: CreateListenerResult<State, Values>;
  disableAutoSubmitting?: boolean;
  enableReinitialize?: boolean;
  validateOnMount?: boolean;
  validateOnChange?: boolean;
  validateOnBlur?: boolean;
  validationSchema?: TypedValidationSchema<State, Values>;
}
