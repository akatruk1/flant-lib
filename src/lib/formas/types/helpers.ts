/* eslint-disable @typescript-eslint/no-explicit-any */
import type { FormasActions } from "./form";
import type { Dispatch } from "redux";

export type ShapeManualActions<M> = M extends Record<string, infer U>
  ? U extends (payload: any) => infer R
    ? R
    : ShapeManualActions<U>
  : never;

export type FormasDispatch<Values, Id extends string> = Dispatch<
  ShapeManualActions<FormasActions<Values, Id>>
>;

export type InferValueByKey<S, K> = K extends keyof S ? S[K] : any;

export type PreviousValues<Values> = Partial<
  Record<keyof Values | string, unknown>
>;

export type UseCacheManagerResult = {
  set: (key: string, value: any) => void;
  get: (key: string) => any;
  isEmpty: () => boolean;
  cleanup: () => void;
};
