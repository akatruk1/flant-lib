/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  getFormasActions,
  getFormasSelectors,
} from "src/lib/formas/helpers/form";

import { useFormasContext } from "./context";
import { Formas } from "./formas";
import { changeFirstLetterCase } from "./helpers";

import type { FunctionComponent } from "react";
import type { FormasContextValues } from "src/lib/formas/types/context";
import type {
  FormasActions,
  FormasSelectors,
  FormasProps,
} from "src/lib/formas/types/form";

type FormasAdapterProps<State, Values, Id extends string> = Omit<
  FormasProps<State, Values, Id>,
  "id" | "initialValues"
> &
  Partial<Pick<FormasProps<State, Values, Id>, "initialValues">>;

type FormasAdapterForm<State, Values, Id extends string> = {
  [Key in string as Capitalize<Id>]: FunctionComponent<
    FormasAdapterProps<State, Values, Id>
  >;
};

type FormasAdapterContext<State, Values, Id extends string> = {
  [Key in string as `use${Capitalize<Id>}`]: () => FormasContextValues<
    State,
    Values,
    Id
  >;
};

type Result<State, Values, Id extends string> = FormasAdapterForm<
  State,
  Values,
  Id
> &
  FormasAdapterContext<State, Values, Id> & {
    actions: FormasActions<Values, Id>;
    selectors: FormasSelectors<State, Values>;
  };

const createFormasAdapter = <State, Values, Id extends string>(
  id: Id,
  schema: Values,
): Result<State, Values, Id> => {
  const capitalizeId = changeFirstLetterCase(id) as Capitalize<Id>;
  const actions = getFormasActions<Values, Id>(id);
  const selectors = getFormasSelectors<State, Values, Id>(id, schema);

  return {
    actions,
    selectors,
    ["use" + capitalizeId]: useFormasContext,
    [capitalizeId]: ({
      initialValues,
      children,
      ...otherProps
    }: FormasAdapterProps<State, Values, Id>) => (
      <Formas<State, Values, Id>
        id={id}
        initialValues={initialValues ?? schema}
        {...otherProps}
      >
        {children}
      </Formas>
    ),
  };
};

export { createFormasAdapter };
