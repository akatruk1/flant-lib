/* eslint-disable @typescript-eslint/no-explicit-any */
import { createContext, useContext } from "react";

import type { FormasContextValues } from "./types";

export const FormasContext = createContext<
  FormasContextValues<any, any, any> | undefined
>(undefined);

export const useFormasContext = <
  State,
  Values,
  Id extends string,
>(): FormasContextValues<State, Values, Id> => {
  const context = useContext(FormasContext);

  if (typeof context === "undefined") {
    throw new Error("Formas Context not initialized");
  }

  return context;
};
