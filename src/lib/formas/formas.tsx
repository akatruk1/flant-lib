/* eslint-disable max-statements */
/* eslint-disable max-lines-per-function */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { getProperty, setProperty } from "dot-prop";
import isEqual from "fast-deep-equal";
import {
  useMemo,
  useCallback,
  useRef,
  useState,
  useEffect,
  useLayoutEffect,
} from "react";
import { useStore } from "react-redux";
import { ValidationError } from "yup";

import { FormasContext } from "./context";
import {
  getRecursiveFields,
  createForm,
  useCacheManager,
  useActionDispatchers,
  cleanupFormasActions,
  cleanupFormasSelectors,
} from "./helpers";
import { useEvent } from "./hooks";
import { LISTENER_EVENTS, ListenerManager } from "./listener";

import type { FormEvent } from "react";
import type {
  FormasDefaultValues,
  FormasErrors,
  FormasTouched,
  FormasProps,
} from "src/lib/formas/types/form";

export const Formas = <
  State,
  Values extends FormasDefaultValues,
  Id extends string,
>({
  id,
  initialValues,
  disableAutoSubmitting = false,
  enableReinitialize = false,
  validationSchema,
  validateOnMount = false,
  validateOnChange = false,
  validateOnBlur = true,
  listeners: listenersBuilder,
  onSubmit,
  children,
}: FormasProps<State, Values, Id>) => {
  const store = useStore<State>();

  const {
    set: setPreviousValue,
    get: getPreviousValue,
    cleanup: cleanupPreviousValues,
  } = useCacheManager();

  const {
    set: setPreviousError,
    get: getPreviousError,
    isEmpty: isEmptyError,
    cleanup: cleanupErrors,
  } = useCacheManager();

  const listenerManagerRef = useRef<ListenerManager<State, Values> | null>(
    null,
  );

  const optionsRef = useRef<
    Pick<
      FormasProps<State, Values, Id>,
      "initialValues" | "validationSchema" | "onSubmit"
    >
  >({
    initialValues,
    validationSchema,
    onSubmit,
  });

  const [loading, setLoading] = useState(true);

  const { actions, reducer, selectors } = useMemo(
    () => createForm<State, Values, Id>(id, initialValues),
    [id, store],
  );

  const {
    handleSetValues,
    handleSetFieldValue,
    handleSetErrors,
    handleSetFieldError,
    handleSetTouched,
    handleSetFieldTouch,
    handleStartSubmit,
    handleEndSubmit,
    handleResetForm,
    dispatch,
  } = useActionDispatchers<Values, Id>(actions);

  const getListenerManager = () => {
    if (!listenerManagerRef.current) {
      listenerManagerRef.current = new ListenerManager();
    }

    return listenerManagerRef.current;
  };

  const runValidate = useEvent(async ({ at, throwError } = {}) => {
    const currentState = store.getState();
    const values = selectors.selectFormValues(currentState);

    if (!optionsRef.current?.validationSchema) {
      return;
    }

    const validationSchema = optionsRef.current.validationSchema(
      values,
      currentState,
    );

    if (at) {
      if (!getRecursiveFields(validationSchema.fields).includes(at)) {
        return;
      }

      try {
        await validationSchema.validateAt(at, values);

        if (getPreviousError(at)) {
          setFieldError(at, undefined);
          setPreviousError(at, undefined);
        }
      } catch (error) {
        if (!isEqual(getPreviousError(at), error.message)) {
          setPreviousError(at, error.message);
          setFieldError(at, error.message);
        }
      }
    } else {
      try {
        await validationSchema.validate(values, {
          abortEarly: false,
        });

        if (isEmptyError()) {
          setErrors({});
        }
      } catch (error: unknown) {
        let errors = {};

        if (error instanceof ValidationError) {
          errors = error.inner.reduce<FormasErrors<Values>>(
            (carry, { path, message }) => {
              if (!path) {
                return carry;
              }

              if (!getProperty(carry, path)) {
                carry = setProperty(carry, path, message);

                setPreviousError(path, message);
              }

              return carry;
            },
            {},
          );
        }

        if (Object.keys(errors).length > 0) {
          setErrors(errors);
        } else {
          cleanupErrors();
        }

        if (throwError) {
          throw error;
        }
      }
    }
  });

  const setValues = useEvent((values, shouldValidate?: boolean) => {
    handleSetValues(values);

    if (shouldValidate || validateOnChange) {
      runValidate();
    }
  });

  const setFieldValue = useEvent(
    (
      key: keyof Values | string,
      value,
      { observable = true, ignoreOriginSet = false } = {},
    ) => {
      if (getPreviousValue(key as string) === value) {
        return;
      }

      if (observable) {
        const triggerOptions = ignoreOriginSet
          ? { key: key as string, value, onSetFieldValue: setPreviousValue }
          : { key: key as string, value };

        getListenerManager().trigger(
          LISTENER_EVENTS.onChangeValue.id,
          triggerOptions,
        );
      }

      if (!ignoreOriginSet) {
        setPreviousValue(key as string, value);
        handleSetFieldValue({ key, value, options: { observable } });
      }

      if (validateOnChange) {
        runValidate({ at: key });
      }
    },
  );

  const setErrors = useCallback(
    (errors: FormasErrors<Values>) => handleSetErrors(errors),
    [],
  );

  const setFieldError = useCallback(
    (key: keyof Values | string, value: any) =>
      handleSetFieldError({ key, value }),
    [],
  );

  const setTouched = useCallback(
    (touched: FormasTouched<Values>) => handleSetTouched(touched),
    [],
  );

  const setFieldTouch = useEvent(
    (key: keyof Values | string, touched = true) => {
      handleSetFieldTouch({ key, value: touched });

      if (validateOnBlur) {
        runValidate({ at: key });
      }
    },
  );

  const resetForm = useCallback(
    (state = optionsRef.current.initialValues as Partial<Values>) =>
      handleResetForm(state),
    [],
  );

  const startSubmit = useCallback(() => handleStartSubmit(), []);
  const endSubmit = useCallback(() => handleEndSubmit(), []);

  const formasHelpers = {
    setValues,
    setFieldValue,
    setErrors,
    setFieldError,
    setTouched,
    setFieldTouch,
    startSubmit,
    endSubmit,
    resetForm,
  };

  const handleChange = useEvent((event) => {
    const { id, name, value } = event.target;

    setFieldValue(id || name, value);
  });

  const handleBlur = useEvent((event) => {
    const { id, name } = event.target;

    setFieldTouch(id || name, true);
  });

  const handleSubmit = useEvent(
    async (event: FormEvent<HTMLFormElement> | MouseEvent) => {
      event.preventDefault();

      const currentState = store.getState();

      const values = selectors.selectFormValues(currentState);
      const touched = selectors.selectFormTouched(currentState);
      const errors = selectors.selectFormErrors(currentState);

      try {
        if (!disableAutoSubmitting) {
          handleStartSubmit();
        }

        await runValidate({ throwError: true });

        await optionsRef.current.onSubmit(
          values,
          { touched, errors, ...formasHelpers },
          store.getState(),
        );
      } catch (error) {
        if (error.name !== "ValidationError") {
          throw error;
        }
      } finally {
        if (!disableAutoSubmitting) {
          handleEndSubmit();
        }
      }
    },
  );

  useEffect(() => {
    const setup = async () => {
      setLoading(true);

      await store.reducerManager.add(id, reducer);

      handleSetValues(optionsRef.current.initialValues);
      setLoading(false);
    };

    setup();

    return () => {
      cleanupPreviousValues();
      cleanupErrors();
      cleanupFormasActions(id);
      cleanupFormasSelectors(id);
      store.reducerManager.remove(id);
    };
  }, [reducer]);

  useEffect(() => {
    const listenerManager = getListenerManager();

    if (!loading) {
      if (validateOnMount) {
        runValidate();
      }
    }

    return store.subscribe(() =>
      listenerManager.trigger(LISTENER_EVENTS.onProvideValue.id),
    );
  }, [store, loading, validateOnMount, optionsRef.current.initialValues]);

  useLayoutEffect(() => {
    if (
      enableReinitialize &&
      !isEqual(initialValues, optionsRef.current.initialValues)
    ) {
      optionsRef.current.initialValues = initialValues;

      setValues(initialValues);
    }
  }, [enableReinitialize, initialValues]);

  useEffect(() => {
    optionsRef.current.validationSchema = validationSchema;
    optionsRef.current.onSubmit = onSubmit;
  }, [validationSchema, onSubmit]);

  useEffect(() => {
    getListenerManager().config = {
      selectors,
      helpers: formasHelpers,
      initialValues: optionsRef.current.initialValues,
      setPreviousValue,
      getPreviousValue,
      dispatch,
      store,
    };
  }, [optionsRef.current.initialValues, dispatch, store]);

  useEffect(() => {
    getListenerManager().listeners = listenersBuilder?.build() ?? [];
  }, [listenersBuilder]);

  const context = useMemo(
    () => ({
      actions,
      selectors,
      setValues,
      setFieldValue,
      setErrors,
      setFieldError,
      setTouched,
      setFieldTouch,
      resetForm,
      startSubmit,
      endSubmit,
      handleChange,
      handleBlur,
      handleSubmit,
    }),
    [actions, selectors],
  );

  const renderChildren = () => {
    if (loading) {
      return null;
    }

    if (typeof children === "function") {
      return children(context);
    }

    return children;
  };

  return (
    <FormasContext.Provider value={context}>
      {renderChildren()}
    </FormasContext.Provider>
  );
};
