/* eslint-disable @typescript-eslint/no-explicit-any */
import { memo, useMemo, useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";

import { useFormasContext } from "./context";
import { getFormasError } from "./helpers";

import type { DefaultFormasFieldProps, InnerFormasFieldProps } from "./types";
import type { JSXElementConstructor, ChangeEvent } from "react";

type Props<P> = P &
  DefaultFormasFieldProps & {
    InputComponent: JSXElementConstructor<
      DefaultFormasFieldProps & InnerFormasFieldProps & P
    >;
  };

const BaseFormasField = <P,>({
  name,
  ignoreOriginSet,
  InputComponent,
  onChange,
  onBlur,
  ...otherProps
}: Props<P>) => {
  const { t } = useTranslation();

  const {
    selectors: { selectFormValue, selectFormTouch, selectFormError },
    setFieldValue,
    handleBlur: handleFormBlur,
  } = useFormasContext();

  const value = useSelector((state) => selectFormValue(state, name));
  const touched = useSelector((state) => selectFormTouch(state, name));
  const originError = useSelector((state) => selectFormError(state, name));

  const handleChange = useCallback(
    (event: ChangeEvent<{ value: any }>) => {
      const { value } = event.target;

      setFieldValue(name, value, { ignoreOriginSet });

      if (onChange) {
        onChange(value);
      }
    },
    [ignoreOriginSet, setFieldValue],
  );

  const handleBlur = useCallback(
    (event: FocusEvent) => {
      handleFormBlur(event);

      if (onBlur) {
        onBlur(event);
      }
    },
    [handleFormBlur],
  );

  const error = useMemo(() => {
    const formasError = getFormasError(originError);

    if (!formasError) {
      return "";
    }

    if (Array.isArray(formasError)) {
      return t(...formasError);
    }

    return formasError;
  }, [originError, t]);

  const inputComponentProps = {
    name,
    value,
    error: touched && error ? error : undefined,
    onChange: handleChange,
    onBlur: handleBlur,
    ...otherProps,
  } as unknown as DefaultFormasFieldProps & InnerFormasFieldProps & P;

  return <InputComponent {...inputComponentProps} />;
};

export const FormasField = memo(BaseFormasField) as typeof BaseFormasField;

BaseFormasField.displayName = "FormasField";
