/* eslint-disable @typescript-eslint/no-explicit-any */
import { useSelector } from "react-redux";

import { useFormasContext } from "./context";

import type {
  DefaultFormasSubmitButtonProps,
  InnerFormasSubmitButtonProps,
} from "./types";
import type { JSXElementConstructor } from "react";

type Props<P> = P &
  DefaultFormasSubmitButtonProps & {
    ButtonComponent: JSXElementConstructor<
      DefaultFormasSubmitButtonProps & InnerFormasSubmitButtonProps & P
    >;
  };

export const FormasSubmitButton = <P,>({
  type = "submit",
  ButtonComponent,
  onClick,
  ...otherProps
}: Props<P>) => {
  const { selectors, handleSubmit } = useFormasContext();

  const submitting = useSelector(selectors.selectIsSubmittingForm);
  const isValid = useSelector(selectors.selectIsValidForm);

  const handleClick = (event: MouseEvent) => {
    if (type === "submit") {
      handleSubmit(event);
    }

    if (onClick) {
      onClick(event);
    }
  };

  const buttonComponentProps = {
    type,
    submitting,
    isValid,
    onClick: handleClick,
    ...otherProps,
  } as unknown as DefaultFormasSubmitButtonProps &
    InnerFormasSubmitButtonProps &
    P;

  return <ButtonComponent {...buttonComponentProps} />;
};
