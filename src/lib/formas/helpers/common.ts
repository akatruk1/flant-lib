import type { ObjectSchema } from "yup";

type InnerSchema = { fields?: ObjectSchema<InnerSchema> };

export const EMPTY_OBJECT = {};

export const shallowEqualByKeys = (
  haystack: Record<string, unknown>,
  needle: Record<string, unknown>,
) => Object.keys(haystack).length === 0 && Object.keys(needle).length === 0;

export const transformKeyToPath = (key: string) => key.split(".");

export const changeFirstLetterCase = (string: string) =>
  string[0].toUpperCase() + string.slice(1);

export const getRecursiveFields = (
  fields: ObjectSchema<InnerSchema>["fields"],
): string[] =>
  Object.entries(fields).reduce((carry, [key, field]) => {
    if (typeof field.fields === "undefined") {
      return [...carry, key];
    }

    const innerFields = getRecursiveFields(field.fields).map(
      (innerFieldName) => `${key}.${innerFieldName}`,
    );

    return [...carry, key, ...innerFields];
  }, [] as string[]);
