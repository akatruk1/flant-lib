/* eslint-disable max-lines-per-function */
/* eslint-disable max-lines */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { createAction, createReducer } from "@reduxjs/toolkit";
import { deleteProperty, getProperty, setProperty } from "dot-prop";
import { useCallback, useRef } from "react";
import { useDispatch } from "react-redux";
import { createSelector } from "reselect";

import {
  EMPTY_OBJECT,
  shallowEqualByKeys,
} from "src/lib/formas/helpers/common";

import { changeFirstLetterCase } from "./common";

import type {
  ExtractPayload,
  FormasActions,
  FormasErrorGeneratedSelector,
  FormasErrors,
  FormasPayloadAction,
  FormasSelectors,
  FormasState,
  FormasTouched,
  FormasTouchedGeneratedSelector,
  FormasValueGeneratedSelector,
  SetFieldErrorPayload,
  SetFieldTouchPayload,
  SetFieldValuePayload,
} from "src/lib/formas/types/form";
import type {
  FormasDispatch,
  UseCacheManagerResult,
} from "src/lib/formas/types/helpers";

type FormCache<Value> = Record<string, Value>;

const createFormasState = <S>(initialValues: S) => {
  const initialTouched = Object.keys(initialValues).reduce(
    (carry, key) => ({
      ...carry,
      [key]: false,
    }),
    {} as FormasTouched<S>,
  );

  const state: FormasState<S> = {
    values: initialValues,
    errors: {},
    touched: initialTouched,
    isValid: true,
    isSubmitting: false,
  };

  return state;
};

const createFormasActions = () => {
  const cache: FormCache<FormasActions<any>> = {};

  const getFormasActions = <Values, Id extends string>(id: Id) => {
    if (!cache[id]) {
      cache[id] = {
        setValues: createAction<Values>(id + "/setValues"),
        setErrors: createAction<FormasErrors<Values>>(id + "/setErrors"),
        setTouched: createAction<FormasTouched<Values>>(id + "/setTouched"),
        setFieldValue: createAction<SetFieldValuePayload<Values>>(
          id + "/setFieldValue",
        ),

        setFieldError: createAction<SetFieldErrorPayload<Values>>(
          id + "/setFieldError",
        ),

        setFieldTouch: createAction<SetFieldTouchPayload<Values>>(
          id + "/setFieldTouch",
        ),

        startSubmit: createAction(id + "/startSubmit"),
        endSubmit: createAction(id + "/endSubmit"),
        resetForm: createAction<Values | undefined>(id + "/resetForm"),
      } as FormasActions<Values, Id>;
    }

    return cache[id] as FormasActions<Values, Id>;
  };

  const cleanupFormasActions = <Id extends string>(id: Id) => {
    delete cache[id];
  };

  return { getFormasActions, cleanupFormasActions };
};

const createFormasSelectors = () => {
  const cache: FormCache<FormasSelectors<any, any>> = {};

  const getFormasSelectors = <
    State extends Record<string, any>,
    Values,
    Id extends string,
  >(
    id: Id,
    schema: Values,
  ) => {
    if (!cache[id]) {
      const selectForm = (state: State) =>
        (state[id] ?? EMPTY_OBJECT) as FormasState<Values>;

      const selectFormValues = createSelector(
        [selectForm],
        (formState) => formState.values ?? schema,
      );

      const selectFormValue = createSelector(
        [selectFormValues, (_, key: keyof Values) => key],
        (values, key) => {
          const typedKey = String(key);

          return getProperty(
            values,
            typedKey,
            getProperty(schema, typedKey, ""),
          );
        },
      ) as unknown as FormasSelectors<State, Values>["selectFormValue"];

      const selectFormErrors = createSelector(
        [selectForm],
        (formState) => formState.errors ?? EMPTY_OBJECT,
      );

      const selectFormError = createSelector(
        [selectFormErrors, (_, key: keyof Values | string) => key],
        (errors, key) => getProperty(errors, String(key)),
      ) as unknown as FormasSelectors<State, Values>["selectFormError"];

      const selectFormTouched = createSelector(
        [selectForm],
        (formState) => formState.touched ?? EMPTY_OBJECT,
      );

      const selectFormTouch = createSelector(
        [selectFormTouched, (_, key: keyof Values | string) => key],
        (touched, key) => getProperty(touched, String(key)),
      ) as unknown as FormasSelectors<State, Values>["selectFormTouch"];

      const selectIsValidForm = (state: State) => selectForm(state).isValid;

      const selectIsSubmittingForm = (state: State) =>
        selectForm(state).isSubmitting;

      const valueGeneratedSelectors = Object.keys(schema).reduce(
        (carry, originKey) => {
          const generatedKey = `selectForm${changeFirstLetterCase(originKey)}`;

          return {
            ...carry,
            [generatedKey]: (key?: string) => (state: State) => {
              const selectKey = key ? originKey + "." + key : originKey;

              return selectFormValue(state, selectKey);
            },
          };
        },
        {} as FormasValueGeneratedSelector<State, Values>,
      );

      const errorGeneratedSelectors = Object.keys(schema).reduce(
        (carry, originKey) => {
          const generatedKey = `selectForm${changeFirstLetterCase(
            originKey,
          )}Error`;

          return {
            ...carry,
            [generatedKey]: (key?: string) => (state: State) => {
              const selectKey = key ? originKey + "." + key : originKey;

              return selectFormError(state, selectKey);
            },
          };
        },
        {} as FormasErrorGeneratedSelector<State, Values>,
      );

      const touchedGeneratedSelectors = Object.keys(schema).reduce(
        (carry, originKey) => {
          const generatedKey = `selectForm${changeFirstLetterCase(
            originKey,
          )}Touch`;

          return {
            ...carry,
            [generatedKey]: (key?: string) => (state: State) => {
              const selectKey = key ? originKey + "." + key : originKey;

              return selectFormTouch(state, selectKey);
            },
          };
        },
        {} as FormasTouchedGeneratedSelector<State, Values>,
      );

      cache[id] = {
        selectForm,
        selectFormValues,
        selectFormValue,
        selectFormErrors,
        selectFormError,
        selectFormTouched,
        selectFormTouch,
        selectIsValidForm,
        selectIsSubmittingForm,
        ...valueGeneratedSelectors,
        ...errorGeneratedSelectors,
        ...touchedGeneratedSelectors,
      };
    }

    return cache[id] as FormasSelectors<State, Values>;
  };

  const cleanupFormasSelectors = <Id extends string>(id: Id) => {
    delete cache[id];
  };

  return { getFormasSelectors, cleanupFormasSelectors };
};

const createForm = <State, Values, Id extends string>(
  id: Id,
  initialValues: Values,
) => {
  const initialState = createFormasState(initialValues);
  const actions = getFormasActions<Values, Id>(id);
  const selectors = getFormasSelectors<State, Values, Id>(id, initialValues);

  const reducer = createReducer(initialState, (builder) =>
    builder
      .addCase(
        actions.setValues.type,
        (state, { payload }: FormasPayloadAction<typeof actions.setValues>) =>
          setProperty(state, "values", payload),
      )
      .addCase(
        actions.setFieldValue.type,
        (
          state,
          { payload }: FormasPayloadAction<typeof actions.setFieldValue>,
        ) => setProperty(state, `values.${String(payload.key)}`, payload.value),
      )
      .addCase(
        actions.setErrors.type,
        (state, { payload }: FormasPayloadAction<typeof actions.setErrors>) => {
          if (
            shallowEqualByKeys(payload, state.errors as FormasErrors<State>)
          ) {
            return state;
          }

          return setProperty(state, "errors", payload);
        },
      )
      .addCase(
        actions.setFieldError.type,
        (
          state,
          { payload }: FormasPayloadAction<typeof actions.setFieldError>,
        ) => {
          const { key, value } = payload;

          const path = `errors.${String(key)}`;
          const currentError = getProperty(state, path);

          if (currentError === value) {
            return state;
          }

          if (typeof value === "undefined" || value === null) {
            deleteProperty(state, path);

            return state;
          }

          return setProperty(state, path, value);
        },
      )
      .addCase(
        actions.setTouched.type,
        (
          state,
          { payload }: FormasPayloadAction<typeof actions.setTouched>,
        ) => {
          if (
            shallowEqualByKeys(
              payload,
              state.touched as Record<string, unknown>,
            )
          ) {
            return state;
          }

          return setProperty(state, "touched", payload);
        },
      )
      .addCase(
        actions.setFieldTouch.type,
        (
          state,
          { payload }: FormasPayloadAction<typeof actions.setFieldTouch>,
        ) => {
          const { key, value } = payload;

          const path = `touched.${String(key)}`;
          const currentTouched = getProperty(state, path);

          if (currentTouched === value) {
            return state;
          }

          return setProperty(state, path, value);
        },
      )
      .addCase(actions.startSubmit.type, (state) =>
        setProperty(state, "isSubmitting", true),
      )
      .addCase(actions.endSubmit.type, (state) =>
        setProperty(state, "isSubmitting", false),
      )
      .addCase(
        actions.resetForm.type,
        (_, { payload }: FormasPayloadAction<typeof actions.resetForm>) =>
          ({
            ...initialState,
            values: { ...payload },
          } as FormasState<Values>),
      )
      .addMatcher(
        (action) =>
          [actions.setErrors.type, actions.setFieldError.type].includes(
            action.type,
          ),
        (state) => {
          const hasErrors =
            Object.keys(getProperty(state, "errors", [])).length !== 0;

          return setProperty(state, "isValid", !hasErrors);
        },
      ),
  );

  return { initialState, actions, reducer, selectors };
};

const useCacheManager = (): UseCacheManagerResult => {
  const previousValuesRef = useRef<Record<string, any>>({});

  const set: UseCacheManagerResult["set"] = useCallback(
    (key, value) => (previousValuesRef.current[key] = value),
    [],
  );

  const get: UseCacheManagerResult["get"] = useCallback(
    (key) => previousValuesRef.current[key],
    [],
  );

  const isEmpty = useCallback(
    () => Object.keys(previousValuesRef.current).length === 0,
    [],
  );

  const cleanup = useCallback(() => (previousValuesRef.current = {}), []);

  return { set, get, isEmpty, cleanup };
};

const useActionDispatchers = <Values, Id extends string>(
  actions: FormasActions<Values, Id>,
) => {
  const dispatch = useDispatch<FormasDispatch<Values, Id> & any>();

  const handleSetValues = (
    value: ExtractPayload<FormasActions<Values, Id>["setValues"]>,
  ) =>
    dispatch({
      type: actions.setValues.type,
      payload: value,
    });

  const handleSetFieldValue = (
    value: ExtractPayload<FormasActions<Values, Id>["setFieldValue"]>,
  ) =>
    dispatch({
      type: actions.setFieldValue.type,
      payload: value,
    });

  const handleSetErrors = (
    value: ExtractPayload<FormasActions<Values, Id>["setErrors"]>,
  ) =>
    dispatch({
      type: actions.setErrors.type,
      payload: value,
    });

  const handleSetFieldError = (
    value: ExtractPayload<FormasActions<Values, Id>["setFieldError"]>,
  ) =>
    dispatch({
      type: actions.setFieldError.type,
      payload: value,
    });

  const handleSetTouched = (
    value: ExtractPayload<FormasActions<Values, Id>["setTouched"]>,
  ) =>
    dispatch({
      type: actions.setTouched.type,
      payload: value,
    });

  const handleSetFieldTouch = (
    value: ExtractPayload<FormasActions<Values, Id>["setFieldTouch"]>,
  ) =>
    dispatch({
      type: actions.setFieldTouch.type,
      payload: value,
    });

  const handleStartSubmit = () =>
    dispatch({
      type: actions.startSubmit.type,
      payload: undefined,
    });

  const handleEndSubmit = () =>
    dispatch({
      type: actions.endSubmit.type,
      payload: undefined,
    });

  const handleResetForm = (
    value: ExtractPayload<FormasActions<Values, Id>["resetForm"]>,
  ) =>
    dispatch({
      type: actions.resetForm.type,
      payload: value,
    });

  return {
    handleSetValues,
    handleSetFieldValue,
    handleSetErrors,
    handleSetFieldError,
    handleSetTouched,
    handleSetFieldTouch,
    handleStartSubmit,
    handleEndSubmit,
    handleResetForm,
    dispatch,
  };
};

const { getFormasActions, cleanupFormasActions } = createFormasActions();
const { getFormasSelectors, cleanupFormasSelectors } = createFormasSelectors();

export {
  createForm,
  useCacheManager,
  useActionDispatchers,
  getFormasActions,
  cleanupFormasActions,
  getFormasSelectors,
  cleanupFormasSelectors,
};
