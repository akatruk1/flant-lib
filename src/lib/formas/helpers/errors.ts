/* eslint-disable @typescript-eslint/no-explicit-any */
import type { TOptions } from "i18next";
import type { FormasError } from "src/lib/formas/types/form";

export const extractFormasErrorKey = (error: FormasError): string => {
  if (typeof error === "object") {
    return error.key;
  }

  return error;
};

export const getFormasError = (
  error: FormasError,
): Record<string, any> | [string, TOptions?] | undefined => {
  if (typeof error === "string") {
    return [error];
  }

  if (typeof error === "object") {
    if ("key" in error && "options" in error) {
      const { key, options } = error;

      return [key, options];
    }

    return error;
  }

  return;
};

export const createFormasError =
  (key: string, options: Record<string, any>) => () => ({
    key,
    options,
  });
