import type { FormasErrors, FormasTouched, FormasBag } from "@lib/formas/types";

type TestBag<V> = Partial<{
  touched: Partial<FormasTouched<V>>;
  errors: Partial<FormasErrors<V>>;
}>;

export const createFormasBagTest = <T>(values: T) =>
  jest.fn(
    (testBag: TestBag<T> = {}): FormasBag<T> => ({
      touched: {
        ...Object.keys(values).reduce(
          (carry, key) => ({ ...carry, [key]: false }),
          {} as FormasBag<T>["touched"],
        ),
        ...testBag.touched,
      },
      errors: {
        ...testBag.errors,
      },
      setValues: jest.fn(),
      setFieldValue: jest.fn(),
      setErrors: jest.fn(),
      setFieldError: jest.fn(),
      setTouched: jest.fn(),
      setFieldTouch: jest.fn(),
      resetForm: jest.fn(),
      startSubmit: jest.fn(),
      endSubmit: jest.fn(),
    }),
  );
