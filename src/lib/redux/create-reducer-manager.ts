import { combineReducers } from "redux";

import type { AnyReducersMapObject, ReducerManager } from "redux";

export const createReducerManager = <M extends AnyReducersMapObject>(
  initialReducers: M,
): ReducerManager<M> => {
  const reducers = { ...initialReducers };

  let combinedReducer = combineReducers<M>(reducers);

  let keysToRemove: string[] = [];

  return {
    getReducerMap: () => reducers,

    reduce: (state, action) => {
      if (state && keysToRemove.length > 0) {
        state = { ...state };

        for (const key of keysToRemove) {
          delete state[key];
        }

        keysToRemove = [];
      }

      return combinedReducer(state, action);
    },

    add: (key, reducer) =>
      new Promise((resolve) => {
        if (!key || reducers[key]) {
          return;
        }

        (reducers as AnyReducersMapObject)[key] = reducer;
        combinedReducer = combineReducers<M>(reducers);

        resolve();
      }),

    remove: (key) =>
      new Promise((resolve) => {
        if (!key || !reducers[key]) {
          return;
        }

        delete reducers[key];

        keysToRemove.push(key);
        combinedReducer = combineReducers<M>(reducers);

        resolve();
      }),
  };
};
