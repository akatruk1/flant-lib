import loadable from "@loadable/component";

import { withInitialAction } from "./with-initial-action";

import type { RouterPageOptions } from "./types";

type Options = RouterPageOptions & { fallbackDelay?: number };

/**
 * @param {string} path Any valid URL path (ex.: /products/:id).
 * @param {string} name Page filename (src/pages/).
 * @param {RouterPageOptions} options
 */
export const page = <T extends string>(
  path: string,
  name: T,
  {
    slices,
    initialActions,
    initialQuery,
    injectOnChange,
    fireActionsOnChange,
    fireQueryOnChange,
    protect = false,
    passWithErrors,
  }: Options = {},
) => {
  const Component = loadable(() => import(`src/pages/${name}`));

  const component =
    slices || initialQuery
      ? withInitialAction(Component, {
          newSlices: slices,
          initialActions,
          initialQuery,
          injectOnChange,
          fireActionsOnChange,
          fireQueryOnChange,
          passWithErrors,
        })
      : Component;

  return {
    path,
    name,
    protect,
    component,
  };
};
