import type { Slice, ActionCreator } from "@reduxjs/toolkit";
import type { UseQuery } from "@reduxjs/toolkit/dist/query/react/buildHooks";

export type RouterPageOptions = {
  //  Array for slices related to page
  slices?: Slice[];
  // Array of action creators thats fire on page init
  initialActions?: ActionCreator<{ type: string }>[];
  // Hook for fetch initial data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  initialQuery?: UseQuery<any>;
  // If set - slice will reinject (remove old store an set new stores) always on render page
  injectOnChange?: boolean;
  // if set - actions will trigger always on mount page
  fireActionsOnChange?: boolean;
  // if set - execute query always on mount page
  fireQueryOnChange?: boolean;
  // only for auth user
  protect?: boolean;
  // pass errors to component when initial query finished with errors
  passWithErrors?: boolean;
};
