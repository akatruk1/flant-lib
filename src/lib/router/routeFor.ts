/* eslint-disable @typescript-eslint/no-explicit-any */
import { routes } from "@config/routes";
import { apiRoutes } from "@modules/shared/api";

type RouteNames = {
  [K in typeof routes[number] as K["name"]]: K["path"];
};

export const baseRouteFor = (url: string, options?: Record<string, any>) =>
  Object.keys(options || {}).reduce(
    (route: string, value) =>
      route.includes(value)
        ? route.replace(`{${value}}`, options?.[value])
        : route,
    url,
  );

export const routeFor = (
  name: keyof RouteNames,
  options?: Record<string, any>,
) => {
  const route = routes.find((route) => route.name === name);
  const url = route ? route.path : name;

  return baseRouteFor(url, options);
};

export const routeForApi = (key: keyof typeof apiRoutes) => {
  const route = apiRoutes[key];

  return route;
};
