import { useEffect, useState } from "react";
import { useLocation, useParams } from "react-router-dom";

import {
  useAppStore,
  appSlice,
  selectCurrentSlices,
  selectHistoryPreviousPathname,
} from "@modules/app/store";
import { Loading } from "@modules/layout/moleculas";
import { PageNotFound } from "@modules/layout/organisms";

import type { RouterPageOptions } from "./types";
import type { LoadableComponent } from "@loadable/component";

type Options = Omit<RouterPageOptions, "slices"> & {
  newSlices: RouterPageOptions["slices"];
};

const { storeInjected, storeRemoved, setHistoryPreviousPathname } =
  appSlice.actions;

const isErrorWithStatus = (error: unknown): error is { status: number } =>
  typeof error === "object" && error !== null && "status" in error;

/**
 * @param {string} Component Loadable component
 * @param {RouterPageOptions} options Options for init page
 */
export const withInitialAction = (
  Component: LoadableComponent<{ data?: unknown; error?: unknown }>,
  {
    newSlices = [],
    initialActions = [],
    initialQuery,
    injectOnChange = false,
    fireActionsOnChange = true,
    fireQueryOnChange = false,
    passWithErrors = false,
  }: Options,
) => {
  const InitialActionComponent = () => {
    const store = useAppStore();
    const params = useParams();
    const { pathname } = useLocation();

    const [loadingStore, setLoadingStore] = useState(true);

    useEffect(() => {
      const previousPathname = selectHistoryPreviousPathname(store.getState());

      if (loadingStore) {
        return;
      }

      const executeActions = () =>
        initialActions.forEach((action) => store.dispatch(action(params)));

      if (!previousPathname || (previousPathname && fireActionsOnChange)) {
        executeActions();
      }
    }, [loadingStore, fireActionsOnChange]);

    useEffect(() => {
      const bulkInjectSlices = async (slices: Options["newSlices"] = []) => {
        const newSlicesName = slices.map((slice) => slice.name);

        await Promise.all(
          slices.map(async ({ name, reducer }) =>
            store.reducerManager.add(name, reducer),
          ),
        );

        store.dispatch(storeInjected(newSlicesName));
      };

      const bulkRemoveSlices = async (names: string[] = []) => {
        await Promise.all(
          names.map(async (name) => store.reducerManager.remove(name)),
        );

        store.dispatch(storeRemoved(names));
      };

      const setupStore = async () => {
        const currentSlices = selectCurrentSlices(store.getState());

        try {
          setLoadingStore(true);

          if (injectOnChange) {
            if (currentSlices.length !== 0) {
              bulkRemoveSlices(currentSlices);
            }

            if (newSlices.length !== 0) {
              bulkInjectSlices(newSlices);
            }

            return;
          }

          if (newSlices.length > currentSlices.length) {
            const difference = newSlices.filter(
              (slice) => !currentSlices.includes(slice.name),
            );

            bulkInjectSlices(difference);
          } else if (currentSlices.length > newSlices.length) {
            const difference = currentSlices.filter((slice) =>
              newSlices.find((newSlice) => newSlice.name !== slice),
            );

            bulkRemoveSlices(difference);
          }
        } finally {
          setLoadingStore(false);
        }
      };

      setupStore();
    }, [injectOnChange]);

    useEffect(() => {
      if (!loadingStore) {
        store.dispatch(setHistoryPreviousPathname(pathname));
      }
    }, [pathname, loadingStore]);

    const {
      data,
      error = {},
      isLoading: loading = false,
    } = initialQuery?.(params, {
      refetchOnMountOrArgChange: fireQueryOnChange,
    }) ?? {};

    if (loadingStore || loading) {
      return <Loading />;
    }

    if (!passWithErrors && isErrorWithStatus(error) && error.status === 404) {
      return <PageNotFound />;
    }

    return <Component data={data} error={error} />;
  };

  return InitialActionComponent;
};
