import { Navigate } from "react-router";

import { routeFor } from "@lib/router";
import { useGetCurrentUserQuery } from "@modules/auth/model";

import type { FunctionComponent, ReactNode } from "react";

type Props = {
  children: ReactNode;
};

export const ProtectedRoute: FunctionComponent<Props> = ({ children }) => {
  const { data, isLoading } = useGetCurrentUserQuery();

  if (isLoading) {
    return null;
  }

  if (!data) {
    return <Navigate replace to={routeFor("login")} />;
  }

  return <>{children}</>;
};
