import loadable from "@loadable/component";
import { NavLink } from "react-router-dom";

import { routes } from "@config/routes";

import type { FunctionComponent, PropsWithChildren } from "react";
import type { NavLinkProps } from "react-router-dom";

type Props = Pick<NavLinkProps, "className"> & {
  to: string;
};

export const PrefetchPageLink: FunctionComponent<PropsWithChildren<Props>> = ({
  to,
  className,
  children,
}) => {
  const { name } =
    routes.find(({ path }) => {
      const raw = "^" + path.replace(/:[a-z_]+/g, ".+") + "/?$";
      const regex = new RegExp(raw);

      return regex.test(to);
    }) || {};

  const Page = name ? loadable(() => import(`src/pages/${name}`)) : null;

  const handleMouseOver = () => Page?.preload();

  return (
    <NavLink to={to} className={className} onMouseOver={handleMouseOver}>
      {children}
    </NavLink>
  );
};
