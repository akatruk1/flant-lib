/// <reference types="react" />

declare module '*.scss' {
  const classes: Record<string, string>
  export default classes
}
declare module '*.svg' {
  const content: React.FunctionComponent<React.PropsWithChildren<React.SVGAttributes<SVGElement>>>
  export default content
}
declare module "*.jpeg" {
  const value: string;
  export default value;
}
declare module "*.jpg" {
  const value: string;
  export default value;
}
declare module "*.png" {
  const value: string;
  export default value;
}
declare module "*.gif" {
  const value: string;
  export default value;
}
