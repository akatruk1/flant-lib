import { createRoot } from "react-dom/client";
import { Helmet } from "react-helmet";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";

import { App } from "@/app";
import "@/i18n";
import { store } from "@modules/app/store";

const root = createRoot(document.getElementById("root") as HTMLElement);

root.render(
  <>
    <Helmet>
      <title>3c Payments</title>
    </Helmet>

    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>
  </>,
);
