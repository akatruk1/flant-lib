import { page } from "@lib/router";

export const routes = [
  page("/", "dashboard", {
    protect: true,
    fallbackDelay: 300,
  }),

  page("/login", "login"),

  page("/dashboard", "dashboard", {
    protect: true,
    passWithErrors: true,
  }),

  page("/oauth/google", "oauth/google"),

  page("*", "404"),
];
