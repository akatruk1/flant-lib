import { CleanWebpackPlugin } from "clean-webpack-plugin";
// eslint-disable-next-line import/default
import CopyPlugin from "copy-webpack-plugin";
import Dotenv from "dotenv-webpack";
import "dotenv/config";
import ESLintPlugin from "eslint-webpack-plugin";
import HtmlWebpackPlugin from "html-webpack-plugin";
import ImageMinimizerPlugin from "image-minimizer-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import StyleLintPlugin from "stylelint-webpack-plugin";
import TerserPlugin from "terser-webpack-plugin";
import TsconfigPathsPlugin from "tsconfig-paths-webpack-plugin";
import webpack from "webpack";
import { BundleAnalyzerPlugin } from "webpack-bundle-analyzer";
import WebpackDashboard from "webpack-dashboard/plugin/index.js";

import { resolve, dirname } from "path";
import { fileURLToPath } from "url";

const isDevelopment = process.env.NODE_ENV !== "production";
const withDashboard = process.env.WEBPACK_CLI === "dashboard";
const withAnalyze = process.env.WEBPACK_ANALYZE === "true";

const __dirname = dirname(fileURLToPath(import.meta.url));

const { API_URL } = process.env;

const webpackConfig = {
  context: resolve(__dirname, "./"),
  devServer: {
    allowedHosts: "all",
    proxy: {
      "/api": {
        target: API_URL,
        secure: false,
        changeOrigin: true,
      },
    },
    static: {
      directory: resolve(__dirname, "public"),
    },
    historyApiFallback: true,
    compress: false,
    port: 9000,
    client: {
      webSocketURL: "ws://localhost:9000/ws",
    },
  },
  entry: {
    bundle: "./src/index.tsx",
  },
  output: {
    path: resolve(__dirname, "./dist"),
    filename: isDevelopment ? "[name].js" : "[name].[fullhash].js",
    chunkFilename: isDevelopment ? "[name].js" : "[name].[chunkhash].js",
    publicPath: "/",
  },
  devtool: isDevelopment ? "source-map" : false,
  stats: {
    assets: true,
    modules: false,
    hash: false,
    children: false,
    warnings: false,
    errorDetails: true,
  },
  mode: isDevelopment ? "development" : "production",
  resolve: {
    plugins: [
      new TsconfigPathsPlugin({ extensions: [".ts", ".tsx", ".js", ".jsx"] }),
    ],

    extensions: [".ts", ".tsx", ".js", ".jsx"],
    alias: {
      src: resolve(__dirname, "./src"),
      "api-schema": resolve(__dirname, "api-schema"),
    },
  },
  optimization: {
    minimize: !isDevelopment,
    minimizer: [
      new TerserPlugin({
        minify: TerserPlugin.swcMinify,
      }),
      new ImageMinimizerPlugin({
        generator: [
          {
            type: "asset",
            implementation: ImageMinimizerPlugin.imageminGenerate,
            options: {
              plugins: [
                ["gifsicle", { optimizationLevel: 3 }],
                ["mozjpeg", { quality: 70 }],
                "imagemin-pngquant",
                [
                  "svgo",
                  {
                    plugins: [
                      {
                        name: "removeViewBox",
                        active: false,
                      },
                    ],
                  },
                ],
              ],
            },
          },
        ],
      }),
    ],
    splitChunks: isDevelopment
      ? false
      : {
          cacheGroups: {
            vendors: {
              test: /[\\/]node_modules[\\/]/,
              priority: -10,
              reuseExistingChunk: true,
            },
            common: {
              minChunks: 2,
              priority: -20,
              reuseExistingChunk: true,
            },
          },
          chunks: "all",
          maxInitialRequests: 30,
          maxAsyncRequests: 30,
          maxSize: 250000,
        },
  },
  plugins: [
    ...(isDevelopment
      ? []
      : [
          new BundleAnalyzerPlugin({
            openAnalyzer: withAnalyze,
            analyzerMode: "static",
          }),
        ]),

    new StyleLintPlugin({
      customSyntax: "postcss-scss",
      context: resolve(__dirname, "./src"),
      failOnError: !isDevelopment,
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[name].css",
    }),
    new webpack.LoaderOptionsPlugin({
      options: { failOnError: !isDevelopment },
    }),
    new ESLintPlugin(),
    new HtmlWebpackPlugin({
      template: "src/templates/index.ejs",
    }),
    new Dotenv({
      path: isDevelopment
        ? "./.env.development"
        : `./.env.${process.env.WERF_ENV}`,
    }),
    ...(isDevelopment
      ? []
      : [
          new CopyPlugin({
            patterns: [{ from: "public", to: "" }],
          }),
        ]),
    new CleanWebpackPlugin(),
    ...(withDashboard ? [new WebpackDashboard()] : []),
  ],
  module: {
    rules: [
      {
        test: /\.(ts|tsx|js|jsx)$/,
        exclude: /node_modules\/(?!@3commas)/,
        loader: "swc-loader",
        options: {
          jsc: {
            parser: {
              syntax: "typescript",
              tsx: true,
              dynamicImport: true,
            },
            transform: {
              react: {
                runtime: "automatic",
              },
            },
          },
        },
      },
      {
        test: /\.(scss|sass|css)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              sourceMap: isDevelopment,
              modules: {
                auto: (resourcePath) => !resourcePath.endsWith(".css"),
                localIdentName: isDevelopment
                  ? "[local]_[hash:base64:5]"
                  : "[hash:base64:5]",
                exportOnlyLocals: false,
                exportLocalsConvention: "camelCaseOnly",
              },
              importLoaders: 2,
            },
          },
          {
            loader: "postcss-loader",
            options: {
              sourceMap: isDevelopment,
            },
          },
          {
            loader: "sass-loader",
            options: {
              sassOptions: {
                outputStyle: "compressed",
                includePaths: [resolve(__dirname, "./src/utils/styles")],
              },
            },
          },
        ],
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: "url-loader",
            options: {
              fallback: "file-loader",
              limit: 8192,
              emitFile: true,
              name: "[name].[hash:8].[ext]",
              outputPath: "images",
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        exclude: /node_modules/,
        loader: "svg-react-loader",
      },
    ],
  },
};

export default webpackConfig;
